@echo off

setlocal enabledelayedexpansion

REM -= Start to sync-up Data schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Client\Data\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Data[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Data[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

endlocal

echo "Sync-data-database (Done)..."