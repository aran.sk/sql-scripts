@echo off

setlocal enabledelayedexpansion

REM -= Start to rollback schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Rollback\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Rollback[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Rollback[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

endlocal

echo "Rollback-schema-database (Done)..."