@echo off

setlocal enabledelayedexpansion

REM -= Start to sync-up Tables schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Tables\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Tables[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Tables[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Columns schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Columns\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Columns[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Columns[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Constraints schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Constraints\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Constraints[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Constraints[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Views schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Views\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Views[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Views[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Types schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Types\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Types[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Types[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Indexes schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Indexs\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Indexs[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Indexs[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Comments schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Comments\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Comments[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Comments[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Sequences schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Sequences\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Sequences[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Sequences[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Functions schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Functions\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Functions[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Functions[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Procedures schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Procedures\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Procedures[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Procedures[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

REM -= Start to sync-up Trigger schema =-
for %%f in (D:\git-runner\KA.SQL.Scripts\KAsset.Aware\Trigger\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "Trigger[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set Trigger[') do (
	D:\git-runner\sqlcl\bin\sql.exe PVDREGISTRA_SQL/PVDREGISTRA@//172.16.48.53:1521/orclpdb @%%f
)

endlocal

echo "Sync-schema-database (Done)..."