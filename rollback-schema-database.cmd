@echo off

setlocal enabledelayedexpansion

for %%f in (.\runner-scripts\Schema-Rollback\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "rollbackfile[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set rollbackfile[') do (
	C:\GitLab-Runner\sqlcl\bin\sql.exe PVDREGISTRA_RUN/PVDREGISTRA@//172.16.48.40:1521/orclpdb @%%f
)

endlocal

echo "Rollback-schema-database (Done)..."