@echo off

setlocal enabledelayedexpansion

for %%f in (.\runner-scripts\Schema-Initiation\*.sql) do (
   for /F "delims=_" %%n in ("%%f") do (
	  set "number=00000%%n"
      set "schemafile[!number:~-6!]=%%f"
   )
)

for /F "tokens=2 delims==" %%f in ('set schemafile[') do (
	C:\GitLab-Runner\sqlcl\bin\sql.exe PVDREGISTRA_RUN/PVDREGISTRA@//172.16.48.40:1521/orclpdb @%%f
)

endlocal

echo "Sync-schema-database (Done)..."