--------------------------------------------------------------------------
-- Play this script in PVDREGISTRA_TEST@172.16.48.40:1521/ORCLPDB to make it look like PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB
--
-- Please review the script before using it to make sure it won't
-- cause any unacceptable data loss.
--
-- PVDREGISTRA_TEST@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 
-- PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 

CREATE OR REPLACE TRIGGER TRG_EMAIL_LOGS 
BEFORE INSERT
ON PVDREGISTRA.EMAIL_LOGS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column EL_ID
  :new.EL_ID := SEQ_EMAIL_ID.nextval;
END TRG_EMAIL_LOGS;
/
SHOW ERRORS;

CREATE OR REPLACE TRIGGER UPDATE_EMPYEE_DMRI 
AFTER UPDATE
OF EMPNAME_THAI
ON PVDREGISTRA.EMPLOYER 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
BEGIN
    update employee set empyee_name=:NEW.EMPNAME_THAI where empyer_id=:OLD.EMPYER_ID and empyee_status in ('DMI','DMK');
  
    update employee set empyee_name=:NEW.EMPNAME_THAI where empyer_id=:OLD.EMPYER_ID and empyee_status in ('DMR') and empyer_id not in (20174098,2016975,20182843,20174341,20174378,20174333,20174364,20174339,20174343,20174373) ;
END;
/
SHOW ERRORS;

EXIT;