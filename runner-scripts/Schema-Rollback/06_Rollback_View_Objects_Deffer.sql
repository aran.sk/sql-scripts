--------------------------------------------------------------------------
-- Play this script in PVDREGISTRA@172.16.48.40:1521/ORCLPDB to make it look like PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB
--
-- Please review the script before using it to make sure it won't
-- cause any unacceptable data loss.
--
-- PVDREGISTRA@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 
-- PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 

-- No action taken.  This is a column of a view.  
-- Changes should be made in underlying objects of the view, not the view itself.

CREATE OR REPLACE FORCE VIEW V_MONEY_TYPE
(MONEYTYPE_ID, MONEYTYPE_NAME, MONEYTYPE_GROUPREPORT, MONEYTYPE_RECEIVE, MONEYTYPE_GROUPREPORT_ID, 
 GROUPSHOWEXCLENEW)
BEQUEATH DEFINER
AS 
with  et as (
    select * from TABLE_LINE et WHERE tbl_code = 'MoneyType'
),ex as (
    select * from TABLE_LINE et WHERE tbl_code = 'MoneyTypeExcelNew'
) select et.tbl_id MoneyType_id,
          et.tbl_desc MoneyType_name,
          et.TBL_VALUE MoneyType_GroupReport,
          et.TBL_VALUE_2 MoneyType_Receive,
          et.TBL_VALUE_3 MoneyType_GroupReport_ID,
          ex.tbl_desc GroupShowExcleNew          
from et,ex where et.tbl_id=ex.tbl_master_id;

CREATE OR REPLACE FORCE VIEW V_PAYMENT_TYPE
(PAYMENT_ID, PAYMENT_NAME, PAYMENT_GROUP, PAYMENT_GROUP_NAME)
BEQUEATH DEFINER
AS 
WITH paytype
        AS (SELECT et.tbl_id Payment_id,
                   et.tbl_desc Payment_name,
                   et.tbl_value Payment_Group_id
              FROM TABLE_LINE et
             WHERE tbl_code = 'Payment'),
        paymentgrp
        AS (SELECT et1.tbl_id Payment_Group_id,
                   et1.tbl_desc Payment_Group_name
              FROM TABLE_LINE et1
             WHERE tbl_code = 'Payment_Group')
   SELECT paytype.PAYMENT_ID,
          PAYMENT_NAME,
          paytype.Payment_Group_id PAYMENT_GROUP,
          Payment_Group_name
     FROM paytype, paymentgrp
    WHERE paytype.Payment_Group_id = paymentgrp.Payment_Group_id;


COMMENT ON TABLE V_USER_EMPYER IS '';

EXIT;