--------------------------------------------------------------------------
-- Play this script in PVDREGISTRA@172.16.48.40:1521/ORCLPDB to make it look like PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB
--
-- Please review the script before using it to make sure it won't
-- cause any unacceptable data loss.
--
-- PVDREGISTRA@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 
-- PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 

DROP INDEX VERIFY_TP_PROGRESS_PK;

DROP INDEX VERIFY_FINAL_PROGRESS_PK;

DROP INDEX VERIFY_APPROVE_LOT_PK;

DROP INDEX UNQ_ESTIMATE_PAY_HEADER_ESTIMATE_PAY_ID;

DROP INDEX TRANS_PAYMENT_DETAIL_PK;

DROP INDEX TRANS_OVERCON_PAYMENT_PK;

DROP INDEX TRANS_OVERCON_LOT_PK;

DROP INDEX TRANS_OVERCON_HEADER_PK;

DROP INDEX TRANS_OVERCON_DETAIL_PK;

DROP INDEX TRANS_CH_OVERDUE_STATUS;

DROP INDEX PK_VERIFY_UNVESTED_DETAIL_VERIFY_UNVESTED_DETAIL_ID;

DROP INDEX PK_VERIFY_TAX_VERIFY_TAX_ID;

DROP INDEX PK_VERIFY_TAX_RECAL_ERROR_VERIFY_TAX_RECALERR_ID;

DROP INDEX PK_VERIFY_RECAL_TRANS_TAX_ERROR_TAX_RECALTRANSERR_ID;

DROP INDEX PK_VERIFY_LOT_MAP_PAYMENT_VERIFY_APPROVE_LOT_ID;

DROP INDEX PK_VERIFY_FINAL_APPROVE_LOT_VERIFY_FINAL_APPROVE_LOT_ID;

DROP INDEX PK_VERIFY_ESTIMATE_PAY_DETAIL_VERIFY_ESTIMATE_PAY_DETAIL_ID;

DROP INDEX PK_VERIFY_CONFLICT_ERROR_DETAIL_VERIFY_CONFLICT_ERROR_DETAIL_ID;

DROP INDEX PK_TRANS_TAX_TRANS_TAX_ID;

DROP INDEX PK_TRANS_REIMBURSE_TRANS_REIMBURSE_ID;

DROP INDEX PK_TRANS_REIMBURSE_DOCUMENT_TRANS_REIMBURSE_DOCUMENT_ID;

DROP INDEX PK_TRANS_FEE_HEADER_TRANS_FEE_HEADER_ID;

DROP INDEX PK_TRANS_FEE_DETAIL_TRANS_FEE_DETAIL_ID;

DROP INDEX PK_TRANS_CHG_PAYMENT_MANAGEMENT_TRANS_CHG_PAYMENT_ID;

DROP INDEX PK_TRANS_CHEQUE_OVERDUE_HEADER_TRANS_CHEQUE_OVERDUE_ID;

DROP INDEX PK_TRANS_CHEQUE_OVERDUE_DETAIL_TRANS_CHEQUE_OVERDUE_ID;

DROP INDEX PK_TAX_ALLOCATE_TAX_ALLOCATE_ID;

DROP INDEX PK_NEW_PAYMENT_REF;

DROP INDEX PK_GROUP_EMAIL_FUND_GROUP_EMAIL_FUND_ID;

DROP INDEX PK_FILETAX_FOR_VERIFY_FILETAX_FOR_VERIFY_ID;

DROP INDEX PK_ESTIMATE_PAY_DETAIL_ESTIMATE_PAY_ID;

DROP INDEX PK_EMAIL_TEMPLATE_EMAIL_TEMPLATE_ID;

DROP INDEX PK_EMAIL_LIST_EMAIL_ID;

DROP INDEX PK_EMAIL_GRP_FUND_TEMPLATE_EMAIL_GRP_FUND_TEMPLATE_ID;

DROP INDEX PK_EMAIL_FUND_EMAIL_FUND_ID;

DROP INDEX PK_DETAIL_ERROR_FINAL_APPROVE_LOT_DETAIL_ERROR_ID;

DROP INDEX PK_DETAIL_BNF_ERR_FINALAPPROVE_DETAIL_BNF_ERR_FINAL_ID;

DROP INDEX PK_CHEQUE_INFO_CHQ_INFO_ID;

DROP INDEX PK_AUDIT_TRANS_REIMBURSE_AUDIT_TRANS_REIMBURSE_ID;

DROP INDEX PAYMENT_APPROVE_LOT_PK;

DROP INDEX MATCH_TRANS_PAYMENT_PK;

DROP INDEX IDX_VER_UNVEST_DETAIL;

DROP INDEX IDX_VER_TRANS_TAX;

DROP INDEX IDX_VER_TP_DETAIL;

DROP INDEX IDX_VER_TAX_HEADER;

DROP INDEX IDX_VER_RE_TRANS_TAX;

DROP INDEX IDX_VER_EST_PAY;

DROP INDEX IDX_VER_ESTIMATE_PAY;

DROP INDEX IDX_VERIFY_TAX_FILETAX_FOR_VERIFY_ID;

DROP INDEX IDX_VERIFY_FINAL_APPROVE_LOT_VERIFY_TP_HEADER_ID;

DROP INDEX IDX_VERIFY_FINAL_APPROVE_LOT_PAYMENT_LOT_ID;

DROP INDEX IDX_VERIFY_CONFLICT_ERROR_DETAIL_VERIFY_TP_HEADER_ID;

DROP INDEX IDX_VERIFY_APPROVE_LOT_FUND_PAYMENT_APPROVE_LOT_ID;

DROP INDEX IDX_TRAN_OVC_PAY_LOTID;

DROP INDEX IDX_TRAN_OVC_PAY_DEL;

DROP INDEX IDX_TRANS_RIM_DOC;

DROP INDEX IDX_TRANS_RIM_BANK;

DROP INDEX IDX_TRANS_REIMBURSE_STM_ID;

DROP INDEX IDX_TRANS_REIMBURSE_EMPYER_ID;

DROP INDEX IDX_TRANS_REIMBURSE_BANK_ID;

DROP INDEX IDX_TRANS_REIMBURSE_ACC_NO;

DROP INDEX IDX_TRANS_PAYMENT_CHQ_INFO_ID;

DROP INDEX IDX_TRANS_FEE_ST;

DROP INDEX IDX_TRANS_FEE_HEADER;

DROP INDEX IDX_TRANS_FEE_FD;

DROP INDEX IDX_TRANS_CHEQUE_OVERDUE;

DROP INDEX IDX_TAX_ALLOCATE_VERIFY_APPROVE_LOT_ID;

DROP INDEX IDX_TAX_ALLOCATE_EMPYEE_TAX_ID;

DROP INDEX IDX_NEW_PAYMENT_REF_TRANS_CHG_PAYMENT_ID;

DROP INDEX IDX_NEW_PAYMENT_REF_OLD_TRANS_PAYMENT_ID;

DROP INDEX IDX_NEW_PAYMENT_REF_NEW_TRANS_PAYMENT_ID;

DROP INDEX IDX_MATCH_TRANS_DETAIL;

DROP INDEX IDX_LOG_PRO_KA_GRP_EYER_ID;

DROP INDEX IDX_FUND_PAYMENT_APPROVE_LOT_PAYMENT_LOT_ID;

DROP INDEX IDX_EST_PAY_DETAIL;

DROP INDEX IDX_ESTIMATE_PAY_HEADER_FUND_ID;

DROP INDEX IDX_EMPYEE_TAX_ALLOCATE;

DROP INDEX IDX_EMAIL_GRP_FUND_TEMPLATE_GROUP_EMAIL_FUND_ID;

DROP INDEX IDX_EMAIL_GRP_FUND_TEMPLATE_EMAIL_TEMPLATE_ID;

DROP INDEX IDX_EMAIL_FUND_GROUP_EMAIL_FUND_ID;

DROP INDEX IDX_DPC_FUND_ID;

DROP INDEX IDX_DETAIL_ERROR_FINAL_APPROVE_LOT_VERIFY_FINAL_APPROVE_LOT_ID;

DROP INDEX IDX_DETAIL_BNF_ERR_FINALAPPROVE_TRANS_PAYMENT_ID;

DROP INDEX IDX_DETAIL_BNF_ERR_FINALAPPROVE_DETAIL_ERROR_ID;

DROP INDEX IDX_DESC_PAY_CLAIM_STATUS;

DROP INDEX IDX_DESC_PAY_CLAIM_NAV_DATE;

DROP INDEX IDX_DESC_PAY_CLAIM_ID;

DROP INDEX IDX_CONEMPYEE_EMPYEE_TYPE;

DROP INDEX IDX_CLAIM_RECEIVE_TRAN_MAIN_ID;

DROP INDEX IDX_CHEQUE_INFO_TRANS_CHEQUE_OVERDUE_ID;

DROP INDEX IDXCON_EMPYEE_EMPYEE_STATUS;

DROP INDEX FUND_PAYMENT_APPROVE_LOT_PK;

DROP INDEX FUND_ACCOUNT_PK;

DROP INDEX ESTIMATE_PAY_HEADER_PK;

DROP INDEX AUDIT_TRANS_PAYMENT_PK;

DROP INDEX AUDIT_TRANS_OVERCON_PAYMENT_PK;

DROP INDEX AUDIT_TRANS_OVERCON_LOT_PK;

DROP INDEX AUDIT_TRANS_OVERCON_HEADER_PK;

DROP INDEX AUDIT_TRANS_OVERCON_DETAIL_PK;

EXIT;