--------------------------------------------------------------------------
-- Play this script in PVDREGISTRA@172.16.48.40:1521/ORCLPDB to make it look like PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB
--
-- Please review the script before using it to make sure it won't
-- cause any unacceptable data loss.
--
-- PVDREGISTRA@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 
-- PVDREGISTRA_SQL@172.16.48.40:1521/ORCLPDB Schema Extracted by User SYSTEM 

DROP TYPE TRANS_FEE_PAY_T_TABLE;

DROP TYPE TRANS_FEE_PAY_T_RECORD;

DROP TYPE TRANS_FEE_CHECK_T_TABLE;

DROP TYPE TRANS_FEE_CHECK_T_RECORD;

DROP TYPE TRANS_FEE_CHECK_DE_T_TABLE;

DROP TYPE TRANS_FEE_CHECK_DE_T_RECORD;

EXIT;