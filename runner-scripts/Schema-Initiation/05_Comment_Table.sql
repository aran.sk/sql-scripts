-- BEGIN SCRIPT --
COMMENT ON COLUMN DESC_PAY_CLAIM.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN DESC_PAY_CLAIM.APPROVE_H_USER_ID IS 'รหัสผู้สร้างรายการ (Section Head)';

COMMENT ON COLUMN DESC_PAY_CLAIM.APPROVE_M_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN DESC_PAY_CLAIM.APPROVE_M_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN DESC_PAY_CLAIM.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN DESC_PAY_CLAIM.DESC_PAY_CLAIM_ID IS 'รหัสรายการ Claim หลังจัดสรร';

COMMENT ON COLUMN DESC_PAY_CLAIM.DESC_PAY_CLAIM_STATUS IS 'สถานะรายการค่าธรรมเนียม :: :: I=เริ่มบันทึกรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN DESC_PAY_CLAIM.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN DESC_PAY_CLAIM.NOTE IS 'Note ต่าง ๆ';

COMMENT ON COLUMN DESC_PAY_CLAIM.REJECT_NOTE IS 'สาเหตุการ Reject';

COMMENT ON COLUMN DESC_PAY_CLAIM.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';

COMMENT ON COLUMN DESC_PAY_CLAIM.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, M=Manager, H=Section Head';

COMMENT ON TABLE EMPYEE_CUR_BALANCE IS 'ล่าสุดสมาชิกที่ขอรับเงินงวด บอกว่าอยู่กองไหน';

COMMENT ON TABLE EMPYEE_PERIOD_RETIRE IS 'การรับเงินเป็น เดือน ดูว่าเดือนไหน ต้อง Genarate\n\n(ลาออกมานานแล้ว)';

COMMENT ON COLUMN EMPYEE_TAX.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN EMPYEE_TAX.EMPYEE_TAX_ID IS 'รหัส TAX ของสมาชิกที่ถูกสร้างหลังจัดสรร  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMPYEE_TAX.IS_PAYTO_REVENUE IS 'สถานะการทำรายการจ่ายเงินให้แก่สรรพากร \n:: Y=จัดทำรายการแล้ว,N=ยังไม่ได้จัดทำรายการ';

COMMENT ON COLUMN EMPYEE_TAX.IS_TAX_ALLOCATE IS 'สถานะว่า Head Approve ได้อนุมัติการกันเงินภาษีแล้ว\n:: Y=อนุมัติแล้ว,N=ยังไม่ได้อนุมัติ';

COMMENT ON COLUMN EMPYEE_TAX.REJECT_NOTE IS 'สาเหตุการ Reject ไม่กันเงินภาษีล่าสุด';

COMMENT ON COLUMN EMPYEE_TAX.REJECT_NOTE_H IS 'สาเหตุการ Reject ไม่กันเงินภาษีล่าสุด';

COMMENT ON COLUMN EMPYEE_TAX.TRANS_TAX_ID IS 'รหัส Transaction ภาษี  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON TABLE EMPYEE_VESTING IS 'ข้อมูลการจ่ายเงินให้สมาชิกที่ลาออกก่อนประมวลผล แยกตามกอง';

COMMENT ON COLUMN FUND_ACCOUNT.IS_DEFAULT_INSTRUCTION IS 'ใช้สำหรับกำหนดค่าการทำ Instruction หากเป็น ''Y'' จำเป็นต้อนทำ หากเป็น ''N'' ไม่จำเป็นต้องทำ';

COMMENT ON COLUMN FUND_ACCOUNT.IS_HAVE_STATEMENT IS 'ไว้สำหรับตรวจสอบว่ามีข้อมูลการโอนเงินหรือไม่ หากเป็น ''''Y'''' (มี),  ''N'' (ไม่มี)';

COMMENT ON COLUMN FUND_AMC.FUND_TYPE IS 'ประเภทของกองทุน';

COMMENT ON TABLE INSTALLMENT_EMPLOYEE IS 'จ่ายเงินงวด(หลังจัด)';

COMMENT ON COLUMN INSTALLMENT_TRANSACTION.REQ_AMT IS 'จำนวนเงินที่ขอ';

COMMENT ON TABLE INSTALLMENT_TRANSACTION IS 'Transaction จ่ายเงินงวด';

COMMENT ON TABLE OVER_RESIGN IS 'รายการขอคืนเงิน เนื่องจากส่งเงินมาเกินแต่ถูกจัดสรรไปแล้ว';

COMMENT ON TABLE OVER_RESIGN_REG_DESC IS 'รายละเอียดขอคืนเงิน เนื่องจากส่งเงินมาเกินแต่ถูกจัดสรรไปแล้ว แยกตามกองและ AMC';

COMMENT ON COLUMN STATEMENT_DESC.REMAIN_BALANCE IS 'ยอดเงินคงเหลือ';

COMMENT ON COLUMN STATEMENT_HD.IMPORT_DATE IS 'วันที่ Load ข้อมูลเข้าระบบ';

COMMENT ON TABLE DAILY_NAV IS 'ข้อมูล NAV \nTD =   ISALLOT = ''Y''';

COMMENT ON COLUMN EMPYEE_TAX.FUND_ID IS 'รหัสนโยบาย';

EXIT;