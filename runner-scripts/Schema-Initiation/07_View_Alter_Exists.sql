-- BEGIN SCRIPT --
CREATE OR REPLACE FORCE VIEW V_PAYMENT_TYPE
(PAYMENT_ID, PAYMENT_NAME, PAYMENT_GROUP, PAYMENT_GROUP_NAME)
BEQUEATH DEFINER
AS 
WITH paytype AS (
   SELECT
        et.tbl_id Payment_id,
       et.tbl_desc Payment_name,
       et.tbl_value Payment_Group_id 
   FROM TABLE_LINE et 
   WHERE
        tbl_code = 'Payment'),
   paymentgrp AS (
   SELECT
        et1.tbl_id Payment_Group_id,
       et1.tbl_desc Payment_Group_name 
   FROM TABLE_LINE et1 
   WHERE
        tbl_code = 'Payment_Group') 
SELECT
    paytype.PAYMENT_ID,
   PAYMENT_NAME,
   paytype.Payment_Group_id PAYMENT_GROUP,
   Payment_Group_name 
FROM paytype,
    paymentgrp 
WHERE
    paytype.Payment_Group_id = paymentgrp.Payment_Group_id 
order by
    paytype.Payment_Group_id,
    paytype.PAYMENT_ID;

COMMENT ON TABLE V_PAYMENT_TYPE IS 'ประเภทการจ่ายเงิน ตามกลุ่ม';

EXIT;