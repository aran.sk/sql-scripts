-- BEGIN SCRIPT --
ALTER TABLE CON_EMPYEE
 ADD (IS_LOCK_TRANSACTION  VARCHAR2(1 BYTE));

ALTER TABLE CON_EMPYEE
 ADD (IS_LOCK_USER  VARCHAR2(10 BYTE));

ALTER TABLE CON_EMPYEE
 ADD (IS_LOCK_DATETIME  DATE);

ALTER TABLE DESC_PAY_CLAIM
 ADD (DESC_PAY_CLAIM_ID  NUMBER);

ALTER TABLE DESC_PAY_CLAIM
 ADD (DESC_PAY_CLAIM_STATUS  VARCHAR2(2 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (REJECT_STEP  VARCHAR2(1 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (DELETE_STEP  VARCHAR2(1 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (APPROVE_M_USER_ID  VARCHAR2(10 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (APPROVE_M_UPDATE_DATETIME  DATE);

ALTER TABLE DESC_PAY_CLAIM
 ADD (APPROVE_H_USER_ID  VARCHAR2(10 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (APPROVE_H_UPDATE_DATETIME  DATE);

ALTER TABLE DESC_PAY_CLAIM
 ADD (REJECT_NOTE  NVARCHAR2(1000));

ALTER TABLE DESC_PAY_CLAIM
 ADD (NOTE  VARCHAR2(3000 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (REJECT_NOTE_H  VARCHAR2(3000 BYTE));

ALTER TABLE DESC_PAY_CLAIM
 ADD (EDIT_TICKET  VARCHAR2(56 BYTE));

ALTER TABLE EMPYEE_TAX
 ADD (EMPYEE_TAX_ID  NUMBER);

ALTER TABLE EMPYEE_TAX
 ADD (IS_PAYTO_REVENUE  VARCHAR2(1 BYTE));

ALTER TABLE EMPYEE_TAX
 ADD (IS_TAX_ALLOCATE  VARCHAR2(1 BYTE));

ALTER TABLE EMPYEE_TAX
 ADD (REJECT_NOTE  VARCHAR2(3000 BYTE));

ALTER TABLE EMPYEE_TAX
 ADD (REJECT_NOTE_H  VARCHAR2(3000 BYTE));

ALTER TABLE EMPYEE_TAX
 ADD (TRANS_TAX_ID  NUMBER);

ALTER TABLE EMPYEE_TAX
 ADD (EDIT_TICKET  VARCHAR2(56 BYTE));

ALTER TABLE FUND_ACCOUNT
 ADD (IS_HAVE_STATEMENT  NVARCHAR2(1)               DEFAULT 'N');

ALTER TABLE FUND_ACCOUNT
 ADD (IS_DEFAULT_INSTRUCTION  NVARCHAR2(1)          DEFAULT 'N');

ALTER TABLE FUND_AMC
 ADD (FUND_TYPE  VARCHAR2(3 BYTE));

ALTER TABLE MENU_APP_KA
 ADD (IS_NEW_APP  VARCHAR2(1 BYTE)                  DEFAULT 'N');

ALTER TABLE MENU_APP_KA
 ADD (MENU_ICON  VARCHAR2(30 BYTE));

ALTER TABLE STATEMENT_DESC
 ADD (REMAIN_BALANCE  NUMBER);

ALTER TABLE STATEMENT_HD
 ADD (IMPORT_DATE  DATE);

ALTER TABLE TABLE_LINE
MODIFY(TBL_VALUE_3 VARCHAR2(1000 BYTE));

ALTER TABLE TOAD_PLAN_TABLE
MODIFY(OBJECT_NAME VARCHAR2(1000 BYTE));
 
EXIT;