-- BEGIN SCRIPT --
CREATE TABLE AUDIT_TRANS_OVERCON_DETAIL
(
  TRANS_OVERCON_HEADER_ID        NUMBER,
  TRANS_OVERCON_PAYMENT_ID       NUMBER,
  TBL_ID_CONTRIBUTION_PART       VARCHAR2(2 BYTE),
  AMOUNT                         NUMBER,
  IS_DELETED                     VARCHAR2(2 BYTE),
  USER_ID                        VARCHAR2(10 BYTE),
  UPDATE_DATETIME                DATE,
  AUDIT_TRANS_OVERCON_DETAIL_ID  NUMBER,
  OPERATION_FLAG                 VARCHAR2(2 BYTE),
  STEP_FLAG                      VARCHAR2(2 BYTE),
  AUDIT_DATETIME                 TIMESTAMP(6)
);

CREATE TABLE AUDIT_TRANS_OVERCON_HEADER
(
  TRANS_OVERCON_HEADER_ID        NUMBER,
  TBL_ID_OVERCON_TYPE            VARCHAR2(2 BYTE),
  SEQ_ID                         NUMBER,
  TRANS_OVERCON_LOT_ID           NUMBER,
  CON_ID                         NUMBER,
  EMPYEE_ID                      NUMBER,
  EMPYER_AMT                     NUMBER,
  EMPYEE_AMT                     NUMBER,
  TBL_ID_PAY_TO                  VARCHAR2(2 BYTE),
  EMPYEE_STATUS                  VARCHAR2(3 BYTE),
  OVERCON_HEADER_STATUS          VARCHAR2(1 BYTE),
  USER_ID                        VARCHAR2(10 BYTE),
  UPDATE_DATETIME                DATE,
  EDIT_TICKET                    NUMBER,
  AUDIT_TRANS_OVERCON_HEADER_ID  NUMBER,
  OPERATION_FLAG                 VARCHAR2(2 BYTE),
  STEP_FLAG                      VARCHAR2(2 BYTE),
  AUDIT_DATETIME                 TIMESTAMP(6)
);

CREATE TABLE AUDIT_TRANS_OVERCON_LOT
(
  TRANS_OVERCON_LOT_ID        NUMBER,
  CREATE_DATE                 TIMESTAMP(6),
  NAV_DATE                    DATE,
  FUND_ID                     NUMBER,
  EMPYER_ID                   NUMBER,
  NOTE                        VARCHAR2(1000 CHAR),
  OVERCON_STATUS              VARCHAR2(1 BYTE),
  REJECT_STEP                 VARCHAR2(1 BYTE),
  DELETE_STEP                 VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID           VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME   TIMESTAMP(6),
  APPROVE_H_USER_ID           VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME   TIMESTAMP(6),
  USER_ID                     VARCHAR2(10 BYTE),
  UPDATE_DATETIME             TIMESTAMP(6),
  EDIT_TICKET                 VARCHAR2(56 BYTE),
  AUDIT_TRANS_OVERCON_LOT_ID  NUMBER,
  OPERATION_FLAG              VARCHAR2(2 BYTE),
  STEP_FLAG                   VARCHAR2(2 BYTE),
  AUDIT_DATETIME              TIMESTAMP(6)
);

CREATE TABLE AUDIT_TRANS_OVERCON_PAYMENT
(
  TRANS_OVERCON_PAYMENT_ID        NUMBER,
  TRANS_OVERCON_LOT_ID            NUMBER,
  SEQ_ID                          NUMBER,
  EMPYEE_ID                       NUMBER,
  PAYEE_NAME                      VARCHAR2(500 BYTE),
  TBL_ID_PAYTYPE                  VARCHAR2(50 BYTE),
  BANK_ID                         VARCHAR2(4 BYTE),
  ACC_NO                          VARCHAR2(15 BYTE),
  AMOUNT                          NUMBER,
  PAYMENT_DATE                    DATE,
  NOTE                            VARCHAR2(1000 CHAR),
  PAY_TO                          VARCHAR2(2 BYTE),
  IS_DELETED                      VARCHAR2(2 BYTE),
  USER_ID                         VARCHAR2(10 BYTE),
  UPDATE_DATETIME                 DATE,
  AUDIT_TRANS_OVERCON_PAYMENT_ID  NUMBER,
  OPERATION_FLAG                  VARCHAR2(2 BYTE),
  STEP_FLAG                       VARCHAR2(2 BYTE),
  AUDIT_DATETIME                  TIMESTAMP(6)
);

CREATE TABLE AUDIT_TRANS_PAYMENT
(
  TRANS_PAYMENT_ID        NUMBER,
  CHQ_INFO_ID             NUMBER,
  NAV_DATE                DATE,
  PAYMENT_TYPE            VARCHAR2(3 BYTE),
  TBL_ID_PAYTYPE          VARCHAR2(50 BYTE),
  PAYMENTCREATETYPE       VARCHAR2(2 BYTE),
  ACC_NO_PAY              VARCHAR2(15 BYTE),
  BANK_ID_PAY             VARCHAR2(3 BYTE),
  PAYDATE                 DATE,
  NAV_DATE_ALLOT          DATE,
  BENEFIT_NAME            VARCHAR2(1000 BYTE),
  AMOUNT                  NUMBER,
  AMOUNT_INIT             NUMBER,
  AMOUNT_DISBURSE         NUMBER,
  IS_AMOUNT_INCLUDE_FEE   VARCHAR2(1 BYTE),
  PAYMENT_FEE             VARCHAR2(2 BYTE),
  PAYMENT_STATUS          VARCHAR2(2 BYTE),
  NOTE                    VARCHAR2(500 BYTE),
  IS_CANCEL               VARCHAR2(1 BYTE),
  TBL_ID_REASON_CANCEL    VARCHAR2(50 BYTE),
  IS_PAY                  VARCHAR2(1 BYTE),
  IS_APPROVE              VARCHAR2(1 BYTE),
  APPROVE_DATETIME        DATE,
  APPROVE_USER_ID         VARCHAR2(10 BYTE),
  CREATE_DATETIME         DATE,
  CREATE_USER_ID          VARCHAR2(10 BYTE),
  USER_ID                 VARCHAR2(10 BYTE),
  UPDATE_DATETIME         DATE,
  EMPYER_ID               NUMBER,
  EMPYEE_ID               NUMBER,
  FUND_ID                 NUMBER,
  REJECT_STEP             VARCHAR2(1 BYTE),
  REJECT_NOTE_H           VARCHAR2(3000 BYTE),
  REJECT_NOTE_Z           VARCHAR2(3000 BYTE),
  IS_BNF_RECIVE           VARCHAR2(1 BYTE),
  PAY_DATE_RECEIVE        DATE,
  AUDIT_TRANS_PAYMENT_ID  NUMBER,
  OPERATION_FLAG          VARCHAR2(2 BYTE),
  STEP_FLAG               VARCHAR2(2 BYTE),
  AUDIT_DATETIME          TIMESTAMP(6),
  IS_PAYMENT_RECORD       VARCHAR2(1 BYTE)
);

COMMENT ON TABLE AUDIT_TRANS_PAYMENT IS 'รายละเอียดของการจ่ายเงิน';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.TRANS_PAYMENT_ID IS 'รหัสTransaction หลักที่ทำรายการต่าง ๆ  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.CHQ_INFO_ID IS 'รหัสTransaction ข้อมูลเช็ค  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.NAV_DATE IS 'ข้อมูลประจำ TD ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAYMENT_TYPE IS 'ประเภทรายการ :: CHQ=เช็ค,TRF=โอนเงิน';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.TBL_ID_PAYTYPE IS '"รหัส ประเภทการขอเงินคืน
CAP = เช็คขีดคร่อม Account Payee Only
CCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)
CCO = เช็ค  ที่ขีดฆ่าผู้ถือ
TNF = โอนเงิน"';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAYMENTCREATETYPE IS 'ประเภทรายการเช็คว่ามีแหล่งที่มาจากที่ใด :: KA=KA ออกเอง ,TF=รับโอนจากบลจ. อื่น ,KT=รับโอนจากกองอื่นภายใต้ บลจ KA';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.ACC_NO_PAY IS 'เลขที่บัญชีจ่าย ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.BANK_ID_PAY IS 'ธนาคารบัญชีจ่าย';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAYDATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.NAV_DATE_ALLOT IS 'TD ที่จัดสรรรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.BENEFIT_NAME IS 'ชื่อผู้รับ ผลประโยชน์';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.AMOUNT IS 'จำนวนเงินที่ต้องจ่าย (กรณีโอนผิดมากกว่า 1 รอบยอดเงินอาจจะลดลงถ้าสมาชิกเป็นผู้รับผิดชอบค่าโอน)';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.AMOUNT_INIT IS 'จำนวนเงินที่ต้องจ่ายเริ่มต้น';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.AMOUNT_DISBURSE IS 'จำนวนเงินที่เบิกจ่ายไปจากธนาคาร';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.IS_AMOUNT_INCLUDE_FEE IS 'จำนวนที่ต้องการโอนรวมค่า Fee  แล้วหรือไม่ ถ้ารวมจะจ่ายสมาชิกเต็มจำนวน โดยถือว่ากองทุนเป็นผู้ออก :: Y =รวม ;N= ไม่รวม';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAYMENT_FEE IS 'ค่าธรรมเนียมการจ่ายเงิน - กรณีจ่ายด้วยเช็ค :: MR=สมาชิกเป็นผู้รับผิดชอบ ,FR=เป็นค่าใช้จ่ายกองทุน';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAYMENT_STATUS IS '"สถานะรายการจ่าย :: 
I=รออนุมัติรายการเปลี่ยนการจ่ายเงิน
H=Head Section Approve 
Z=ผู้บริหารอนุมัติ 
R=Reject"';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.NOTE IS 'เหตุผลการยกเลิก หรือ Reject';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.IS_CANCEL IS 'สถานะการยกเลิกรายการ กรณีออกรายการ Payment ใหม่ :: Y=ยกเลิก , N=ไม่ยกเลิก';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.TBL_ID_REASON_CANCEL IS '"เหตุผลการยกเลิก
CHGC=เปลี่ยนแปลงการจ่ายเงิน จากโอนเป็นเช็ค 
CHGT=เปลี่ยนแปลงการจ่ายเงิน จากเช็ค เป็นโอน
CHOV = เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่"';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.IS_PAY IS 'ผู้รับผลประโยชน์ได้รับเงินแล้ว (ถ้าเป็นเช็ค คือ ขึ้นเงิน ถ้าเป็นโอน คือ โอนเงินสำเร็จ) :: Y=ได้รับแล้ว ,N=ยังไม่ได้รับ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.IS_APPROVE IS 'อนุม้ติรายการจ่ายเงิน';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.APPROVE_DATETIME IS 'วันที่อนุมัติรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.CREATE_USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.EMPYER_ID IS 'รหัสนายจ้าง';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.REJECT_NOTE_H IS 'สาเหตุการ Reject ของ Section Head';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.REJECT_NOTE_Z IS 'สาเหตุการ Reject ของ ผู้บริหาร';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.IS_BNF_RECIVE IS 'ผู้รับผลประโยชน์ได้รับเงินแล้ว (ถ้าเป็นเช็ค คือ ขึ้นเงิน ถ้าเป็นโอน คือ โอนเงินสำเร็จ) :: Y=ได้รับแล้ว ,N=ยังไม่ได้รับ';

COMMENT ON COLUMN AUDIT_TRANS_PAYMENT.PAY_DATE_RECEIVE IS 'วันที่สมาชิกรับเงิน กรณีเป็นเช็ค คือวันที่นำเช็คมาขึ้นเงิน จะได้ตอน Map Statement ถ้าเป็นโอนจะเป็นวันที่รับโอน';


CREATE TABLE AUDIT_TRANS_REIMBURSE
(
  AUDIT_TRANS_REIMBURSE_ID   NUMBER,
  TRANS_REIMBURSE_ID         NUMBER,
  NAV_DATE                   DATE,
  EMPYEE_ID                  NUMBER,
  EMPYER_ID                  NUMBER,
  FUND_ID                    NUMBER,
  HAVE_STATEMENT             VARCHAR2(1 BYTE),
  INSTRUCTION_FLAG           VARCHAR2(1 BYTE),
  BANK_ID                    VARCHAR2(4 BYTE),
  ACC_NO                     VARCHAR2(15 BYTE),
  REIMBURSE_REASON           NVARCHAR2(1000),
  TRANSFER_DATE              DATE,
  STM_ID                     VARCHAR2(50 BYTE),
  TOTAL_AMT                  NUMBER,
  TBL_ID_PAYTYPE             VARCHAR2(50 BYTE),
  PAYEE_NAME                 VARCHAR2(1000 BYTE),
  BANK_ID_PAY                VARCHAR2(4 BYTE),
  ACC_NO_PAY                 VARCHAR2(15 BYTE),
  AMOUNT                     NUMBER,
  PAYMENT_DATE               DATE,
  NOTE                       VARCHAR2(1000 CHAR),
  REIMBURSE_STATUS           VARCHAR2(2 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  HAVE_AMT_UNALLOCATE        VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  OPERATION_FLAG             VARCHAR2(2 BYTE),
  STEP_FLAG                  VARCHAR2(2 BYTE),
  AUDIT_DATETIME             TIMESTAMP(6)
);

COMMENT ON TABLE AUDIT_TRANS_REIMBURSE IS 'Table Audit Transaction การขอคืนเงินออกจากบัญชี';

COMMENT ON COLUMN AUDIT_TRANS_REIMBURSE.PAYEE_NAME IS 'ชื่อผู้รับเงิน';


CREATE TABLE CHEQUE_INFO
(
  CHQ_INFO_ID              NUMBER,
  TRANS_CHEQUE_OVERDUE_ID  NUMBER,
  CHQ_NO                   NUMBER,
  IS_CHQ_EXPIRED           VARCHAR2(1 BYTE)     DEFAULT 'N',
  IS_ACCPAYABLE            VARCHAR2(1 BYTE)     DEFAULT 'N',
  TRANS_ACCPAYABLE_LOT_ID  NUMBER,
  IS_STOPCHQ               VARCHAR2(1 BYTE)     DEFAULT 'N',
  IS_DISBURSE              VARCHAR2(1 BYTE),
  IS_NEWCHQ                VARCHAR2(1 BYTE),
  IS_PRINTCHQBYREGISTRA    VARCHAR2(1 BYTE),
  IS_CANCEL                VARCHAR2(1 BYTE),
  TBL_ID_REMARK            VARCHAR2(50 BYTE),
  CHQ_STATUS               VARCHAR2(1 BYTE),
  APPROVE_DATETIME         DATE,
  APPROVE_USER_ID          VARCHAR2(10 BYTE),
  CREATE_DATETIME          DATE,
  CREATE_USER_ID           VARCHAR2(10 BYTE),
  USER_ID                  VARCHAR2(10 BYTE),
  UPDATE_DATETIME          DATE,
  PRINT_DATE               DATE,
  ORDER_ID                 VARCHAR2(250 BYTE),
  BANK_ID                  VARCHAR2(3 BYTE),
  ACC_NO                   VARCHAR2(13 BYTE),
  BRANCH_ID                NUMBER
);

COMMENT ON TABLE CHEQUE_INFO IS 'รายละเอียดของเช็ค';

COMMENT ON COLUMN CHEQUE_INFO.CHQ_INFO_ID IS 'รหัสTransaction ข้อมูลเช็ค  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN CHEQUE_INFO.TRANS_CHEQUE_OVERDUE_ID IS 'รหัสการตั้งเจ้าหนี้';

COMMENT ON COLUMN CHEQUE_INFO.CHQ_NO IS 'เลขที่เช็ค';

COMMENT ON COLUMN CHEQUE_INFO.IS_CHQ_EXPIRED IS 'เช็คหมดอายุครบกำหนด 180 วัน :: Y = หมดอายุแล้ว ,N ยังไม่หมดอายุ';

COMMENT ON COLUMN CHEQUE_INFO.IS_ACCPAYABLE IS 'ตั้งเจ้าหนี้แล้วหรือไม่ :: Y=ตั้งเจ้าหนี้แล้ว , N=ยังไม่ได้ตั้งเจ้าหนี้';

COMMENT ON COLUMN CHEQUE_INFO.TRANS_ACCPAYABLE_LOT_ID IS 'รหัสรายการ ตั้งเจ้าหนี้เพื่อดูว่ากรณีเช็คหมดอายุ ถูกตั้งเจ้าหนี้และโอนเงินแล้วหรือไม่';

COMMENT ON COLUMN CHEQUE_INFO.IS_STOPCHQ IS 'สั่งอายัดเช็คกรณีเช็คเสีย :: Y=สั่งอายัดแล้ว , N= ยังไม่ได้สั่งอายัด';

COMMENT ON COLUMN CHEQUE_INFO.IS_DISBURSE IS 'ขึ้นเงินแล้วหรือไม่ :: Y=ขึ้นเงินแล้ว,N=ยังไม่ขึ้นเงิน';

COMMENT ON COLUMN CHEQUE_INFO.IS_NEWCHQ IS 'กรณียกเลิกเช็ค มีการออกเช็คใหม่หรือไม่ :: Y=ออกเช็คใหม่ ,N=ยังไม่ได้ออกเช็คใหม่';

COMMENT ON COLUMN CHEQUE_INFO.IS_PRINTCHQBYREGISTRA IS 'ทาง KA พิมพ์เช็คด้วยตัวเอง :: Y=พิมพ์เอง , N=ส่งโรงพิมพ์';

COMMENT ON COLUMN CHEQUE_INFO.IS_CANCEL IS 'เช็คถูกยกเลิก หรือเช็คเสีย :Y=เสีย ,N=เช็คปกติ';

COMMENT ON COLUMN CHEQUE_INFO.TBL_ID_REMARK IS 'สาเหตุของเช็คเสีย';

COMMENT ON COLUMN CHEQUE_INFO.CHQ_STATUS IS 'สถานะเช็ค :: \nWN=เลขที่เช็คถูกสร้างจากการยืนยันเช็คเข้าคลัง\nWP=เลขที่เช็คถูกจองในใบเบิกและรอการสั่งพิมพ์\nPR=สั่งพิมพ์เช็ค.\nPC = ยืนยันการสั่งพิมพ์';

COMMENT ON COLUMN CHEQUE_INFO.APPROVE_DATETIME IS 'วันที่อนุมัติรายการ';

COMMENT ON COLUMN CHEQUE_INFO.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการ';

COMMENT ON COLUMN CHEQUE_INFO.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN CHEQUE_INFO.CREATE_USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN CHEQUE_INFO.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN CHEQUE_INFO.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN CHEQUE_INFO.PRINT_DATE IS 'วันที่พิมพ์เช็ค';

COMMENT ON COLUMN CHEQUE_INFO.ORDER_ID IS 'เลขที่คำสั่งซื้อ :: [IV] + [พ.ศ] + [เดือน] + [Auto Run เลข 4 หลัก]';

COMMENT ON COLUMN CHEQUE_INFO.BANK_ID IS 'รหัสธนาคารของเช็ค';

COMMENT ON COLUMN CHEQUE_INFO.ACC_NO IS 'เลขที่บัญชีของเช็ค';

COMMENT ON COLUMN CHEQUE_INFO.BRANCH_ID IS 'สาขาบัญชีของเช็ค';


CREATE TABLE EMAIL_FUND
(
  EMAIL_FUND_ID        NUMBER,
  GROUP_EMAIL_FUND_ID  NUMBER,
  EMAIL_ID             NUMBER,
  FUND_ID              NUMBER,
  USER_ID              VARCHAR2(10 BYTE),
  UPDATE_DATETIME      DATE
);

COMMENT ON TABLE EMAIL_FUND IS 'email ที่ใช้ส่งในแต่ละกองทุน';

COMMENT ON COLUMN EMAIL_FUND.EMAIL_FUND_ID IS 'รหัส email ที่ใช้ส่งในแต่ละกองทุน :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_FUND.GROUP_EMAIL_FUND_ID IS 'รหัสกลุ่มของกองทุนที่ใช้ส่ง Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_FUND.EMAIL_ID IS 'รหัสข้อมูล Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_FUND.FUND_ID IS 'รหัสกองทุน';

COMMENT ON COLUMN EMAIL_FUND.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN EMAIL_FUND.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE EMAIL_GRP_FUND_TEMPLATE
(
  EMAIL_GRP_FUND_TEMPLATE_ID  NUMBER,
  EMAIL_TEMPLATE_ID           NUMBER,
  GROUP_EMAIL_FUND_ID         NUMBER,
  EMAIL_SENDER_FROM           VARCHAR2(250 BYTE),
  EMAIL_SENDER_CC             VARCHAR2(250 BYTE),
  EMAIL_SENDER_BCC            VARCHAR2(250 BYTE),
  USER_ID                     VARCHAR2(10 BYTE),
  UPDATE_DATETIME             DATE
);

COMMENT ON TABLE EMAIL_GRP_FUND_TEMPLATE IS 'กลุ่ม Template ของการส่ง Email';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.EMAIL_GRP_FUND_TEMPLATE_ID IS 'รหัสกลุ่ม Template ของการส่ง Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.EMAIL_TEMPLATE_ID IS 'รหัสข้อมูล Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.GROUP_EMAIL_FUND_ID IS 'รหัสกลุ่มของกองทุนที่ใช้ส่ง Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.EMAIL_SENDER_FROM IS 'Email From ว่าส่งจากใคร';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.EMAIL_SENDER_CC IS 'Email CC';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.EMAIL_SENDER_BCC IS 'Email BCC';

COMMENT ON COLUMN EMAIL_GRP_FUND_TEMPLATE.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE EMAIL_LIST
(
  EMAIL_ID         NUMBER,
  EMAIL_ADDRESS    VARCHAR2(250 BYTE),
  USER_ID          VARCHAR2(10 BYTE),
  UPDATE_DATETIME  DATE
);

COMMENT ON TABLE EMAIL_LIST IS 'Email ทั้งหมดในระบบ';

COMMENT ON COLUMN EMAIL_LIST.EMAIL_ID IS 'รหัสข้อมูล Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_LIST.EMAIL_ADDRESS IS 'ข้อมูล Email';

COMMENT ON COLUMN EMAIL_LIST.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN EMAIL_LIST.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE EMAIL_TEMPLATE
(
  EMAIL_TEMPLATE_ID  NUMBER,
  EMAIL_SUBJECT      VARCHAR2(500 BYTE),
  CONTENT_TEMPLATE   BLOB,
  TEMPLATE_TYPE      VARCHAR2(3 BYTE),
  USER_ID            VARCHAR2(10 BYTE),
  UPDATE_DATETIME    DATE
);

COMMENT ON TABLE EMAIL_TEMPLATE IS 'รูปแบบ Content ในการส่ง Email';

COMMENT ON COLUMN EMAIL_TEMPLATE.EMAIL_TEMPLATE_ID IS 'รหัสข้อมูล Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN EMAIL_TEMPLATE.EMAIL_SUBJECT IS 'หัวข้อ Mail';

COMMENT ON COLUMN EMAIL_TEMPLATE.CONTENT_TEMPLATE IS 'Html Template';

COMMENT ON COLUMN EMAIL_TEMPLATE.TEMPLATE_TYPE IS 'ประเภทของ Template ::\nEST = Estimate การจ่ายเงิน\nSGL = GL';

COMMENT ON COLUMN EMAIL_TEMPLATE.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN EMAIL_TEMPLATE.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE ESTIMATE_PAY_DETAIL
(
  ESTIMATE_PAY_ID          NUMBER,
  FUND_ID                  NUMBER,
  AMC_ID                   NUMBER,
  NAV                      NUMBER,
  NAV_PER_UNIT             NUMBER,
  CLAIM_AMT                NUMBER,
  RETAIN_BALANCE_AMT       NUMBER,
  RECEIVE_INSTALLMENT_AMT  NUMBER,
  TRANSFER_IN_AMT          NUMBER,
  NET_AMT                  NUMBER,
  MARK_UP_AMT              NUMBER,
  ESTIMATE_PAY_TOTAL       NUMBER,
  OVERRESIGN_DONATE_AMT    NUMBER,
  ESTIMATE_PAY_DETAIL_ID   NUMBER
);

COMMENT ON TABLE ESTIMATE_PAY_DETAIL IS 'รายการตรวจสอบข้อมูลการจ่ายเงินหลังจัดสรร';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.ESTIMATE_PAY_ID IS 'รหัสการทำ Estimate ส่งยอดเงินบัญชี :: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.FUND_ID IS 'รหัส sub fund';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.AMC_ID IS 'รหัส บลจ.';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.NAV IS 'NAV';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.NAV_PER_UNIT IS 'ราคาต่อหน่วย';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.CLAIM_AMT IS 'จำนวนเงินจ่ายพ้นสภาพ';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.RETAIN_BALANCE_AMT IS 'จำนวนเงินจ่ายคนคงเงิน';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.RECEIVE_INSTALLMENT_AMT IS 'จำนวนเงินจ่ายคนรับเงินงวด';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.TRANSFER_IN_AMT IS 'จำนวนเงินจ่ายรับโอน';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.NET_AMT IS 'Net Amount';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.MARK_UP_AMT IS 'ยอดประมาณการ 5%';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.ESTIMATE_PAY_TOTAL IS 'ยอดเงินประมาณจ่ายที่ส่งบัญชี';

COMMENT ON COLUMN ESTIMATE_PAY_DETAIL.OVERRESIGN_DONATE_AMT IS 'จำนวนเงินจ่าย Over Resign และเงินบริจาค';


CREATE TABLE ESTIMATE_PAY_HEADER
(
  ESTIMATE_PAY_ID    NUMBER,
  FUND_ID            NUMBER,
  NAV_DATE           DATE,
  IS_APPROVE_TO_USE  VARCHAR2(1 BYTE),
  APPROVE_DATETIME   DATE,
  APPROVE_USER_ID    VARCHAR2(10 BYTE),
  CREATE_DATETIME    DATE,
  USER_ID            VARCHAR2(10 BYTE),
  UPDATE_DATETIME    DATE
);

COMMENT ON TABLE ESTIMATE_PAY_HEADER IS 'รายการตรวจสอบข้อมูลการจ่ายเงินหลังจัดสรร';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.ESTIMATE_PAY_ID IS 'รหัสการทำ Estimate ส่งยอดเงินบัญชี :: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.NAV_DATE IS 'TD ที่จัดสรร';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.IS_APPROVE_TO_USE IS 'สถานะว่าใช้ส่งยอดให้บัญชีใช่หรือไม่ :: Y =ใช่ ,N=ไม่';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.APPROVE_DATETIME IS 'วันที่อนุมัติรายการส่งยอดบัญชี';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการงยอดบัญชี';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN ESTIMATE_PAY_HEADER.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE FILETAX_FOR_VERIFY
(
  FILETAX_FOR_VERIFY_ID  NUMBER,
  FUND_ID                NUMBER,
  TAX_START_DATE         DATE,
  TAX_END_DATE           DATE,
  TAX_AMOUNT             NUMBER,
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE
);

COMMENT ON TABLE FILETAX_FOR_VERIFY IS 'รายละเอียด  Text File ภาษี ที่ได้จาก บัญชี';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.FILETAX_FOR_VERIFY_ID IS 'รหัสของ Text File ภาษี ที่ได้จาก บัญชี  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.FUND_ID IS 'รหัสกองทุน จะเป็น Subfund กรณีเป็น Master และกองทุนหลักถ้าเป็น Single';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.TAX_START_DATE IS 'วันที่เริ่มต้นทีคิดภาษี';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.TAX_END_DATE IS 'วันที่สิ้นสุดทีคิดภาษี';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.TAX_AMOUNT IS 'จำนวนเงินภาษี';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN FILETAX_FOR_VERIFY.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE FUND_PAYMENT_APPROVE_LOT
(
  FUND_PAYMENT_APPROVE_LOT_ID  NUMBER,
  PAYMENT_LOT_ID               NUMBER,
  FUND_ID                      NUMBER,
  VERIFY_TP_HEADER_ID          NUMBER,
  FLOWPAYMENTMNG_STATUS        VARCHAR2(1 BYTE),
  NOTE                         VARCHAR2(500 BYTE),
  REJECT_STEP                  VARCHAR2(1 BYTE),
  DELETE_STEP                  VARCHAR2(1 BYTE)
);

COMMENT ON TABLE FUND_PAYMENT_APPROVE_LOT IS 'ข้อมูลกองทุนใน Lot การตรวจสอบ (ตรวจสอบและอนุมัติรายกองทุน)';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.FUND_PAYMENT_APPROVE_LOT_ID IS 'เลขที่ข้อมูลกองทุนใน Lot :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.PAYMENT_LOT_ID IS 'เลขที่ lot ของการจัดกลุ่มรายการ Payment:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20191 (ระบุเฉพาะมีรายการ หลังจัดสรร)';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.FLOWPAYMENTMNG_STATUS IS 'สถานะของ Lot :: I=สร้าง Lot เพื่อตรวจสอบ H=Head Section ตรวจสอบและยืนยันรายการ (รายการ Payment ถูกสร้าง) Z=ผู้บริหารอนุมัติ R=Reject X=Delete   เพราะ Reject เฉพาะบางกองเพื่อส่งไป Finall Approve ที่เหลือก่อน';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.NOTE IS 'เหตุผลการ Delete   หรือ Kill';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN FUND_PAYMENT_APPROVE_LOT.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';


CREATE TABLE GROUP_EMAIL_FUND
(
  GROUP_EMAIL_FUND_ID  NUMBER,
  GROUP_EMAIL_NAME     VARCHAR2(250 BYTE),
  USER_ID              VARCHAR2(10 BYTE),
  UPDATE_DATETIME      DATE
);

COMMENT ON TABLE GROUP_EMAIL_FUND IS 'กลุ่มของกองทุนที่ใช้ส่ง Email';

COMMENT ON COLUMN GROUP_EMAIL_FUND.GROUP_EMAIL_FUND_ID IS 'รหัสกลุ่มของกองทุนที่ใช้ส่ง Email :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN GROUP_EMAIL_FUND.GROUP_EMAIL_NAME IS 'ชื่อกลุ่มของกองทุนที่ใช้ส่ง Email';

COMMENT ON COLUMN GROUP_EMAIL_FUND.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN GROUP_EMAIL_FUND.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE HF_AGGREGATED_COUNTER
(
  ID         NUMBER(10),
  KEY        NVARCHAR2(255),
  VALUE      NUMBER(10),
  EXPIRE_AT  TIMESTAMP(4)
);

CREATE TABLE HF_COUNTER
(
  ID         NUMBER(10),
  KEY        NVARCHAR2(255),
  VALUE      NUMBER(10),
  EXPIRE_AT  TIMESTAMP(4)
);

CREATE TABLE HF_DISTRIBUTED_LOCK
(
  "RESOURCE"  NVARCHAR2(100),
  CREATED_AT  TIMESTAMP(4)
);

CREATE TABLE HF_HASH
(
  ID         NUMBER(10),
  KEY        NVARCHAR2(255),
  VALUE      NCLOB,
  EXPIRE_AT  TIMESTAMP(4),
  FIELD      NVARCHAR2(40)
);

CREATE TABLE HF_JOB
(
  ID               NUMBER(10),
  STATE_ID         NUMBER(10),
  STATE_NAME       NVARCHAR2(20),
  INVOCATION_DATA  NCLOB,
  ARGUMENTS        NCLOB,
  CREATED_AT       TIMESTAMP(4),
  EXPIRE_AT        TIMESTAMP(4)
);

CREATE TABLE HF_JOB_PARAMETER
(
  ID      NUMBER(10),
  NAME    NVARCHAR2(40),
  VALUE   NCLOB,
  JOB_ID  NUMBER(10)
);

CREATE TABLE HF_JOB_QUEUE
(
  ID           NUMBER(10),
  JOB_ID       NUMBER(10),
  QUEUE        NVARCHAR2(50),
  FETCHED_AT   TIMESTAMP(4),
  FETCH_TOKEN  NVARCHAR2(36)
);

CREATE TABLE HF_JOB_STATE
(
  ID          NUMBER(10),
  JOB_ID      NUMBER(10),
  NAME        NVARCHAR2(20),
  REASON      NVARCHAR2(100),
  CREATED_AT  TIMESTAMP(4),
  DATA        NCLOB
);

CREATE TABLE HF_LIST
(
  ID         NUMBER(10),
  KEY        NVARCHAR2(255),
  VALUE      NCLOB,
  EXPIRE_AT  TIMESTAMP(4)
);

CREATE TABLE HF_SERVER
(
  ID               NVARCHAR2(100),
  DATA             NCLOB,
  LAST_HEART_BEAT  TIMESTAMP(4)
);

CREATE TABLE HF_SET
(
  ID         NUMBER(10),
  KEY        NVARCHAR2(255),
  VALUE      NVARCHAR2(255),
  SCORE      FLOAT(126),
  EXPIRE_AT  TIMESTAMP(4)
);

CREATE TABLE MATCH_TRANS_PAYMENT
(
  MATCH_TRANS_ID               NUMBER,
  MAIN_OBJECT_TRANS_TYPE       VARCHAR2(4 BYTE),
  OBJECT_TRANS_ID              NUMBER,
  SUB_OBJECT_TRANS_TYPE        VARCHAR2(10 BYTE),
  TRANSACTION_AMOUNT           NUMBER,
  MATCH_PAYMENT_STATUS         VARCHAR2(1 BYTE),
  REJECT_NOTE                  VARCHAR2(3000 BYTE),
  REJECT_STEP                  VARCHAR2(1 BYTE),
  CREATE_DATETIME              DATE,
  APPROVE_H_USER_ID            VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME    DATE,
  APPROVE_Z_USER_ID            VARCHAR2(10 BYTE),
  APPROVE_Z_UPDATE_DATETIME    DATE,
  USER_ID                      VARCHAR2(10 BYTE),
  UPDATE_DATETIME              DATE,
  TRANS_PAYMENT_ID             NUMBER,
  FUND_PAYMENT_APPROVE_LOT_ID  NUMBER,
  TRANSACTION_FEE              NUMBER
);

COMMENT ON TABLE MATCH_TRANS_PAYMENT IS 'รายการ Match ว่ารายการ Payment มาจาก Transaction ใด';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.MATCH_TRANS_ID IS 'รหัสการ Match รายการจ่าย เพื่อออกรายการ Payment กรณี อนุมัติผ่าน:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.MAIN_OBJECT_TRANS_TYPE IS '"ประเภทรายการต้นทางเพื่อจะนำมาออก Payment :: 
RLSP=รายการที่ได้หลังการขายหน่วย(ข้อมูลในตาราง Desc_pay_claim)
APBP = รายการตั้งเจ้าหนี้ (เช็ค Over Due เช็คค้างจ่ายเกิน 6 เดือน ต้องนำส่งผู้ชำระบัญชี เบิกเงินจากบัญชีจ่ายไปบัญชีลงทุน)
OVCN= รายการ Over Con.
RMBP=รายการ Reimburse
CTAX=ภาษีที่ต้องการรวมรวมส่งสรรพากร
PYMP = รายการ Payment เช่น  เปลี่ยนแปลงการจ่ายเงิน เช่นเปลี่ยนจากโอนเป็นเช็ค หรือเปลี่ยนเช็ค หรือ เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่"';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.OBJECT_TRANS_ID IS 'รหัสของประเภทรายการ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.SUB_OBJECT_TRANS_TYPE IS '"รายละเอียดประเภทรายการอธิบายรายการหลัก
1. ALM=รายการที่ได้หลังการจัดสรร (ข้อมูลในตาราง Desc_pay_claim) ประกอบด้วย
   1.1 CLM 
   1.2 CLMO
   1.3 CLM2
   1.4 EXP
   1.5 CLMO2
   1.6 OC
   1.7 RGO
   1.8 OVRS2
   1.9 TMN
   1.10 TAX
   1.11 OVC
   1.12 EYP
   1.13 UNV"';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.TRANSACTION_AMOUNT IS 'จำนวนเงินที่นำไปออก Payment เพื่อจ่ายผู้รับ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.MATCH_PAYMENT_STATUS IS '"สถานะของการ Match_Payment:: \nI : Initail บันทึกรายการ (รายการ Payment ยังไม่ถูกสร้าง)\nH=Head Section ตรวจสอบและยืนยันรายการ (รายการ Payment ถูกสร้าง)\nR=Reject"';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.REJECT_NOTE IS 'สาเหตุการ Reject';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.APPROVE_H_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Section Head)';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.APPROVE_Z_USER_ID IS 'รหัสผู้สร้างรายการ (ผู้บริหาร)';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.APPROVE_Z_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.TRANS_PAYMENT_ID IS 'รหัสรายการการจ่ายเงิน';

COMMENT ON COLUMN MATCH_TRANS_PAYMENT.FUND_PAYMENT_APPROVE_LOT_ID IS 'เลขที่ข้อมูลกองทุนใน Lot :: [ปี ค.ศ.][Running] เช่น 20191';


CREATE TABLE NEW_PAYMENT_REF
(
  OLD_TRANS_PAYMENT_ID  NUMBER,
  NEW_TRANS_PAYMENT_ID  NUMBER,
  TRANS_CHG_PAYMENT_ID  NUMBER,
  USER_ID               VARCHAR2(10 BYTE),
  UPDATE_DATETIME       DATE
);

COMMENT ON TABLE NEW_PAYMENT_REF IS 'รายละเอียดของการจ่ายเงินกรณียกเลิกรายการเดิม เช่นการออกเช็คใหม่ หรือการเปลี่ยนรายการจากโอนเป็นเช็ค หรือจากเช็คเป็นโอน';

COMMENT ON COLUMN NEW_PAYMENT_REF.OLD_TRANS_PAYMENT_ID IS 'รหัสรายการ การจ่ายเงิน เดิม ที่จ่ายออกตอน Payment';

COMMENT ON COLUMN NEW_PAYMENT_REF.NEW_TRANS_PAYMENT_ID IS 'รหัสรายการ การจ่ายเงิน ใหม่ ซึ่งถูกสร้างตอน Head Section Approve';

COMMENT ON COLUMN NEW_PAYMENT_REF.TRANS_CHG_PAYMENT_ID IS 'รหัสรายการ การเปลียนแปลงข้อมูลการจ่ายเงินต่าง ๆ  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN NEW_PAYMENT_REF.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN NEW_PAYMENT_REF.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE PAYMENT_APPROVE_LOT
(
  PAYMENT_LOT_ID             NUMBER,
  PAYMENT_LOT_DETAIL         VARCHAR2(500 BYTE),
  FLOWPAYMENTMNG_STATUS      VARCHAR2(1 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  NAV_DATE                   DATE,
  CREATE_DATETIME            DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  APPROVE_Z_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_Z_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  FINAL_APPROVE_STATUS       VARCHAR2(1 BYTE)
);

COMMENT ON TABLE PAYMENT_APPROVE_LOT IS 'Lot ของ Payment เพื่อทำการอนุมัติและตรวจสอบรายการ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.PAYMENT_LOT_ID IS 'เลขที่ lot ของการจัดกลุ่มรายการ Payment:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.PAYMENT_LOT_DETAIL IS 'คำอธิบาย Lot';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.FLOWPAYMENTMNG_STATUS IS '"สถานะของ Lot :: 
I=สร้าง Lot เพื่อตรวจสอบ
H=Head Section ตรวจสอบและยืนยันรายการ (รายการ Payment ถูกสร้าง)
Z=ผู้บริหารอนุมัติ
R=Reject
X=Delete"';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.NAV_DATE IS 'TD';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.APPROVE_H_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.APPROVE_Z_USER_ID IS 'รหัสผู้สร้างรายการ (Section Head)';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.APPROVE_Z_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN PAYMENT_APPROVE_LOT.FINAL_APPROVE_STATUS IS 'ยังไม่ตรวจสอบ H=Head Section ตรวจสอบและยืนยันรายการ (แจ้งผู้บริหารแล้ว), W=รอประมวลผล, P=กำลังตรวจสอบ, E=Error, F=ระบบตรวจสอบเสร็จแล้ว, Z=ผู้บริหารอนุมัติ Lot, R=ผู้บริหารปฏิเสธ Lot';


CREATE TABLE TAX_ALLOCATE
(
  TAX_ALLOCATE_ID        NUMBER,
  VERIFY_APPROVE_LOT_ID  NUMBER,
  EMPYEE_TAX_ID          NUMBER,
  TAX_AMOUNT             NUMBER,
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE,
  IS_TAX_ALLOCATE        VARCHAR2(1 BYTE),
  REJECT_NOTE            VARCHAR2(3000 BYTE),
  REJECT_NOTE_H          VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE TAX_ALLOCATE IS 'รายละเอียดรายการภาษีที่ถูกนำมากันเงิน';

COMMENT ON COLUMN TAX_ALLOCATE.TAX_ALLOCATE_ID IS 'รหัส Transaction  การตั้งเจ้าหนี้  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TAX_ALLOCATE.VERIFY_APPROVE_LOT_ID IS 'เลขที่ข้อมูลการตรวจสอบรายการ Lot ของ Payment:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TAX_ALLOCATE.EMPYEE_TAX_ID IS 'รหัส TAX ของสมาชิกที่ถูกสร้างหลังจัดสรร  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TAX_ALLOCATE.TAX_AMOUNT IS 'จำนวนเงินภาษีที่ต้องจ่ายของระดับสมาชิก';

COMMENT ON COLUMN TAX_ALLOCATE.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TAX_ALLOCATE.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TAX_ALLOCATE.IS_TAX_ALLOCATE IS 'สถานะว่า Head Approve ได้อนุมัติการกันเงินภาษีแล้ว\n:: Y=อนุมัติแล้ว,N=ยังไม่ได้อนุมัติ';

COMMENT ON COLUMN TAX_ALLOCATE.REJECT_NOTE IS 'สาเหตุการ Reject';

COMMENT ON COLUMN TAX_ALLOCATE.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE TD_FUND_IN_YEAR
(
  TD_FUND_IN_YEAR_ID  NUMBER,
  NAV_DATE            DATE,
  IS_ALLOT            VARCHAR2(1 BYTE)          DEFAULT 'N',
  USER_ID             VARCHAR2(50 BYTE),
  UPDATE_DATETIME     DATE,
  FUND_ID             NUMBER,
  TD_TAX              DATE,
  EDIT_TICKET         VARCHAR2(56 BYTE)
);

COMMENT ON COLUMN TD_FUND_IN_YEAR.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';


CREATE TABLE TD_FUND_IN_YEAR_ALLOT_JOB
(
  TD_FUND_IN_YEAR_ID  NUMBER,
  JOB_ALL_ID          VARCHAR2(10 BYTE),
  USER_ID             VARCHAR2(50 BYTE),
  UPDATE_DATETIME     DATE
);

CREATE TABLE TRANS_CHEQUE_OVERDUE_DETAIL
(
  TRANS_CHEQUE_OVERDUE_ID  NUMBER,
  CHQ_INFO_ID              NUMBER,
  CHQ_SUBFUND              VARCHAR2(2 BYTE),
  FUND_ID                  NUMBER,
  FUND_RATIO               NUMBER,
  FUND_AMOUNT              NUMBER,
  USER_ID                  VARCHAR2(10 BYTE),
  UPDATE_DATETIME          DATE
);

COMMENT ON TABLE TRANS_CHEQUE_OVERDUE_DETAIL IS 'รายละเอียด Transaction ตั้งเจ้าหนี้ ว่าเงินหน้าเช็คต้องถูกแตกสัดส่วน และนำเงินกลับไปบัญชีลงทุนของกองทุนใด';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.TRANS_CHEQUE_OVERDUE_ID IS 'รหัส Transaction  การตั้งเจ้าหนี้  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.CHQ_INFO_ID IS 'รหัส Transaction ข้อมูลเช็ค';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.CHQ_SUBFUND IS 'นโยบายของเงินกรณีเจ้าหน้าเช็คไม่มาขึ้นเงิน :: OS=ให้อยู่ทีนโยบายเดิมก่อนสิ้นสมาชิกภาพ,  MS=นโยบายเสี่ยงต่ำสุด';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.FUND_ID IS 'รหัสกองทุน';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.FUND_RATIO IS 'สัดส่วนของกองทุนตามข้อบังคับ ว่าตามสัดส่วนตอนลาออก หรือตามความเสี่ยงต่ำสุด';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.FUND_AMOUNT IS 'สัดส่วนเงินของกองทุนตามข้อบังคับ ว่าตามสัดส่วนตอนลาออก หรือตามความเสี่ยงต่ำสุด';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE TRANS_CHEQUE_OVERDUE_HEADER
(
  TRANS_CHEQUE_OVERDUE_ID    NUMBER,
  NAV_DATE                   DATE,
  ACC_NO_INVEST              VARCHAR2(15 BYTE),
  BANK_ID_INVEST             VARCHAR2(3 BYTE),
  FUND_ID_INVEST             NUMBER,
  AMC_ID_INVEST              NUMBER,
  ACCOUNTPAYABLE_DATE        DATE,
  BENEFIT_NAME               VARCHAR2(1000 BYTE),
  AMOUNT                     NUMBER,
  ACCPAYABLE_STATUS          VARCHAR2(1 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  TBL_ID_PAYTYPE             VARCHAR2(50 BYTE),
  REJECT_NOTE_H              VARCHAR2(3000 BYTE),
  REJECT_NOTE_Z              VARCHAR2(3000 BYTE),
  PAYDATE                    DATE
);

COMMENT ON TABLE TRANS_CHEQUE_OVERDUE_HEADER IS 'รายละเอียด Transaction ตั้งเจ้าหนี้ ยอดเงินที่จะตั้งเจ้าหนี้โดยโอนจากบัญชีจ่ายไปบัญชีลงทุน';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.TRANS_CHEQUE_OVERDUE_ID IS 'รหัส Transaction  การตั้งเจ้าหนี้  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.NAV_DATE IS 'ข้อมูลประจำ TD';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.ACC_NO_INVEST IS 'เลขที่บัญชีลงทุน (เงินที่ต้องไปโอนเข้าจากบัญชีจ่าย) ***เงินจะเข้าตามบัญชีข้อบ้งคับ เช่น ความเสี่ยงต่ำสุด หรือ เข้าตามกองที่ออก';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.BANK_ID_INVEST IS 'ธนาคารบัญชีลงทุน (เงินที่ต้องไปโอนเข้าจากบัญชีจ่าย)';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.FUND_ID_INVEST IS 'รหัสกองทุนที่โอนเงินเข้า';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.AMC_ID_INVEST IS 'Custodian ผู้ดูแลทรัพยสิน';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.ACCOUNTPAYABLE_DATE IS 'วันที่ตั้งเจ้าหนี้';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.BENEFIT_NAME IS 'ชื่อผู้รับผลประโยชน์';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.AMOUNT IS 'จำนวนเงิน';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.ACCPAYABLE_STATUS IS 'สถานะ  ::  I=เริ่มบันทึกรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.REJECT_STEP IS 'รายการ Reject ถูกยกเลิกกลับมาที่ตำแหน่งใดเป็นผู้แก้ไข :: S=Staff,M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.APPROVE_M_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.APPROVE_M_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.APPROVE_H_USER_ID IS 'รหัสผู้สร้างรายการ (Head Section)';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.TBL_ID_PAYTYPE IS 'รหัส ประเภทการจ่ายเงิน\nCAP = เช็คขีดคร่อม Account Payee Only\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.REJECT_NOTE_H IS 'สาเหตุการ Reject ของ Section Head';

COMMENT ON COLUMN TRANS_CHEQUE_OVERDUE_HEADER.REJECT_NOTE_Z IS 'สาเหตุการ Reject ของ ผู้บริหาร';


CREATE TABLE TRANS_CHG_PAYMENT_MANAGEMENT
(
  TRANS_CHG_PAYMENT_ID   NUMBER,
  TBL_ID_CHG_PAY_TYPE    VARCHAR2(50 BYTE),
  OLD_TRANS_PAYMENT_ID   NUMBER,
  EMPYER_ID              NUMBER,
  EMPYEE_ID              NUMBER,
  FUND_ID                NUMBER,
  NAV_DATE               DATE,
  PAYMENT_TYPE           VARCHAR2(3 BYTE),
  PAYMENTCREATETYPE      VARCHAR2(2 BYTE),
  ACC_NO_PAY             VARCHAR2(15 BYTE),
  BANK_ID_PAY            VARCHAR2(3 BYTE),
  PAYDATE                DATE,
  NAV_DATE_ALLOT         DATE,
  CHQ_INFO_ID            NUMBER,
  TBL_ID_PAYTYPE         VARCHAR2(50 BYTE),
  BENEFIT_NAME           VARCHAR2(1000 BYTE),
  AMOUNT                 NUMBER,
  AMOUNT_INIT            NUMBER,
  IS_AMOUNT_INCLUDE_FEE  VARCHAR2(1 BYTE),
  PAYMENT_FEE            VARCHAR2(2 BYTE),
  APPROVE_DATETIME       DATE,
  APPROVE_USER_ID        VARCHAR2(10 BYTE),
  CREATE_DATETIME        DATE,
  CREATE_USER_ID         VARCHAR2(10 BYTE),
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE,
  CHG_PAYMENT_STATUS     VARCHAR2(3 BYTE),
  CHG_PAYMENT_SSC        VARCHAR2(2 BYTE),
  REJECT_STEP            VARCHAR2(1 BYTE),
  REJECT_NOTE            VARCHAR2(3000 BYTE),
  REJECT_NOTE_H          VARCHAR2(3000 BYTE),
  EDIT_TICKET            VARCHAR2(56 BYTE)
);

COMMENT ON TABLE TRANS_CHG_PAYMENT_MANAGEMENT IS 'รายการเปลี่ยนแปลงรายการ การจ่ายเงินต่าง ๆ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.TRANS_CHG_PAYMENT_ID IS 'รหัสรายการ การเปลียนแปลงข้อมูลการจ่ายเงินต่าง ๆ  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.TBL_ID_CHG_PAY_TYPE IS 'รหัสประเภทการเปลี่ยนแปลงรายการ\nCHGP=เปลี่ยนแปลงการจ่ายเงิน\nCHOV = เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.OLD_TRANS_PAYMENT_ID IS 'รหัสรายการ การจ่ายเงิน เดิม ที่จ่ายออกตอน Payment :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.EMPYER_ID IS 'รหัสนายจ้าง';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.NAV_DATE IS 'ข้อมูลประจำ TD';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.PAYMENT_TYPE IS 'ประเภทรายการ :: CHQ=เช็ค,TRF=โอนเงิน';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.PAYMENTCREATETYPE IS 'ประเภทรายการเช็คว่ามีแหล่งที่มาจากที่ใด :: KA=KA ออกเอง ,TF=รับโอนจากบลจ. อื่น ,KT=รับโอนจากกองอื่นภายใต้ บลจ KA';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.ACC_NO_PAY IS 'เลขที่บัญชีจ่าย';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.BANK_ID_PAY IS 'ธนาคารบัญชีจ่าย';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.PAYDATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.NAV_DATE_ALLOT IS 'TD ที่จัดสรรรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.CHQ_INFO_ID IS 'รหัสTransaction ข้อมูลเช็ค  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.TBL_ID_PAYTYPE IS 'รหัส ประเภทการขอเงินคืน\nCAP = เช็คขีดคร่อม Account Payee Only\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.BENEFIT_NAME IS 'ชื่อผู้รับ ผลประโยชน์';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.AMOUNT IS 'จำนวนเงินที่ต้องจ่าย (กรณีโอนผิดมากกว่า 1 รอบยอดเงินอาจจะลดลงถ้าสมาชิกเป็นผู้รับผิดชอบค่าโอน)';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.AMOUNT_INIT IS 'จำนวนเงินที่ต้องจ่ายเริ่มต้น';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.IS_AMOUNT_INCLUDE_FEE IS 'จำนวนที่ต้องการโอนรวมค่า Fee  แล้วหรือไม่ ถ้ารวมจะจ่ายสมาชิกเต็มจำนวน โดยถือว่ากองทุนเป็นผู้ออก :: Y =รวม ;N= ไม่รวม';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.PAYMENT_FEE IS 'ค่าธรรมเนียมการจ่ายเงิน - กรณีจ่ายด้วยเช็ค :: MR=สมาชิกเป็นผู้รับผิดชอบ ,FR=เป็นค่าใช้จ่ายกองทุน';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.APPROVE_DATETIME IS 'วันที่อนุมัติรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.CREATE_USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.REJECT_NOTE IS 'สาเหตุการ Reject ของ Section Head';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';

COMMENT ON COLUMN TRANS_CHG_PAYMENT_MANAGEMENT.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';


CREATE TABLE TRANS_FEE_DETAIL
(
  TRANS_FEE_DETAIL_ID  NUMBER,
  TRANS_FEE_HEADER_ID  NUMBER,
  DESC_PAY_CLAIM_ID    NUMBER,
  IS_DELETED           VARCHAR2(1 BYTE)         DEFAULT 'N',
  USER_ID              VARCHAR2(10 BYTE),
  UPDATE_DATETIME      DATE,
  EMPYEE_ID            NUMBER
);

COMMENT ON TABLE TRANS_FEE_DETAIL IS 'รายละเอียด Transaction ของค่าธรรมเนียมต่าง ๆ ว่าประกอบมาจากรายการใดบ้าง';

COMMENT ON COLUMN TRANS_FEE_DETAIL.TRANS_FEE_DETAIL_ID IS 'รหัส Transaction ค่าธรรมเนียม  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_FEE_DETAIL.TRANS_FEE_HEADER_ID IS 'รหัส Transaction ค่าธรรมเนียม  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_FEE_DETAIL.DESC_PAY_CLAIM_ID IS 'รหัสรายการ Claim หลังจัดสรร';

COMMENT ON COLUMN TRANS_FEE_DETAIL.IS_DELETED IS 'สถานะรายการว่าถูกลบแล้วหรือไม่ :: Y=ถูกลง,N=ยังไม่ถูกลบ';

COMMENT ON COLUMN TRANS_FEE_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_FEE_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_FEE_DETAIL.EMPYEE_ID IS 'รหัสสมาชิก';


CREATE TABLE TRANS_FEE_HEADER
(
  TRANS_FEE_HEADER_ID        NUMBER,
  TBL_ID_FEE_TYPE            VARCHAR2(50 BYTE),
  FUND_ID                    NUMBER,
  NAV_DATE                   DATE,
  PAYEE_NAME                 VARCHAR2(1000 BYTE),
  TBL_ID_PAYTYPE             VARCHAR2(50 BYTE),
  BANK_ID                    VARCHAR2(5 BYTE),
  ACC_NO                     VARCHAR2(15 BYTE),
  AMOUNT                     NUMBER,
  PAYMENT_DATE               DATE,
  NOTE                       NVARCHAR2(1000),
  TRANS_FEE_STATUS           VARCHAR2(2 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  REJECT_NOTE                VARCHAR2(600 BYTE),
  REJECT_NOTE_H              VARCHAR2(3000 BYTE),
  TRANSACTION_FEE            NUMBER
);

COMMENT ON TABLE TRANS_FEE_HEADER IS 'Transaction ของค่าธรรมเนียมต่าง ๆ';

COMMENT ON COLUMN TRANS_FEE_HEADER.TRANS_FEE_HEADER_ID IS 'รหัส Transaction ค่าธรรมเนียม  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_FEE_HEADER.TBL_ID_FEE_TYPE IS 'รหัสประเภทค่าธรรมเนียม \nEYP = ค่าธรรมเนียมเงินงวด (ที่หักจากสมาชิก)';

COMMENT ON COLUMN TRANS_FEE_HEADER.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN TRANS_FEE_HEADER.NAV_DATE IS 'ข้อมูลประจำ TD';

COMMENT ON COLUMN TRANS_FEE_HEADER.PAYEE_NAME IS 'ชื่อผู้รับเงิน เช่นถ้าเป็นค่าธรรมเนียมเงินงวดจะเป็น "บริษัทหลักทรัพย์จัดการกองทุน กสิกรไทย จำกัด"';

COMMENT ON COLUMN TRANS_FEE_HEADER.TBL_ID_PAYTYPE IS 'รหัส ประเภทการขอเงินคืน\nCAP = เช็คขีดคร่อม Account Payee Only**\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_FEE_HEADER.BANK_ID IS 'รหัสธนาคาร';

COMMENT ON COLUMN TRANS_FEE_HEADER.ACC_NO IS 'เลขที่ บัญชี';

COMMENT ON COLUMN TRANS_FEE_HEADER.AMOUNT IS 'จำนวนเงิน';

COMMENT ON COLUMN TRANS_FEE_HEADER.PAYMENT_DATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_FEE_HEADER.NOTE IS 'Note ต่าง ๆ';

COMMENT ON COLUMN TRANS_FEE_HEADER.TRANS_FEE_STATUS IS 'สถานะรายการค่าธรรมเนียม :: :: I=เริ่มบันทึกรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN TRANS_FEE_HEADER.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, M=Manager, H=Section Head';

COMMENT ON COLUMN TRANS_FEE_HEADER.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_FEE_HEADER.APPROVE_M_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN TRANS_FEE_HEADER.APPROVE_M_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN TRANS_FEE_HEADER.APPROVE_H_USER_ID IS 'รหัสผู้สร้างรายการ (Section Head)';

COMMENT ON COLUMN TRANS_FEE_HEADER.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN TRANS_FEE_HEADER.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_FEE_HEADER.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_FEE_HEADER.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN TRANS_FEE_HEADER.REJECT_NOTE IS 'เหตุผล Reject จาก final';

COMMENT ON COLUMN TRANS_FEE_HEADER.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';

COMMENT ON COLUMN TRANS_FEE_HEADER.TRANSACTION_FEE IS 'จำนวนเงินค่าธรรมเนียม(รายสมาชิก) ขณะที่สร้างรายการ';


CREATE TABLE TRANS_OVERCON_DETAIL
(
  TRANS_OVERCON_HEADER_ID   NUMBER,
  TRANS_OVERCON_PAYMENT_ID  NUMBER,
  TBL_ID_CONTRIBUTION_PART  VARCHAR2(2 BYTE),
  AMOUNT                    NUMBER,
  IS_DELETED                VARCHAR2(2 BYTE),
  USER_ID                   VARCHAR2(10 BYTE),
  UPDATE_DATETIME           DATE
);

CREATE TABLE TRANS_OVERCON_HEADER
(
  TRANS_OVERCON_HEADER_ID  NUMBER,
  TBL_ID_OVERCON_TYPE      VARCHAR2(2 BYTE),
  SEQ_ID                   NUMBER,
  TRANS_OVERCON_LOT_ID     NUMBER,
  CON_ID                   NUMBER,
  EMPYEE_ID                NUMBER,
  EMPYER_AMT               NUMBER,
  EMPYEE_AMT               NUMBER,
  TBL_ID_PAY_TO            VARCHAR2(2 BYTE),
  EMPYEE_STATUS            VARCHAR2(3 BYTE),
  OVERCON_HEADER_STATUS    VARCHAR2(1 BYTE),
  USER_ID                  VARCHAR2(10 BYTE),
  UPDATE_DATETIME          DATE,
  EDIT_TICKET              NUMBER,
  EMPYER_BALANCE_AMT       NUMBER
);

CREATE TABLE TRANS_OVERCON_LOT
(
  TRANS_OVERCON_LOT_ID       NUMBER,
  CREATE_DATE                TIMESTAMP(6),
  NAV_DATE                   DATE,
  FUND_ID                    NUMBER,
  EMPYER_ID                  NUMBER,
  NOTE                       VARCHAR2(1000 CHAR),
  OVERCON_STATUS             VARCHAR2(1 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  TIMESTAMP(6),
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  TIMESTAMP(6),
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            TIMESTAMP(6),
  EDIT_TICKET                VARCHAR2(56 BYTE)
);

CREATE TABLE TRANS_OVERCON_PAYMENT
(
  TRANS_OVERCON_PAYMENT_ID  NUMBER,
  TRANS_OVERCON_LOT_ID      NUMBER,
  SEQ_ID                    NUMBER,
  EMPYEE_ID                 NUMBER,
  PAYEE_NAME                VARCHAR2(1000 BYTE),
  TBL_ID_PAYTYPE            VARCHAR2(50 BYTE),
  BANK_ID                   VARCHAR2(4 BYTE),
  ACC_NO                    VARCHAR2(15 BYTE),
  AMOUNT                    NUMBER,
  PAYMENT_DATE              DATE,
  NOTE                      VARCHAR2(1000 CHAR),
  PAY_TO                    VARCHAR2(2 BYTE),
  IS_DELETED                VARCHAR2(2 BYTE),
  USER_ID                   VARCHAR2(10 BYTE),
  UPDATE_DATETIME           DATE,
  REJECT_STEP               VARCHAR2(1 BYTE),
  IS_RECORD_REJECT_H        VARCHAR2(1 BYTE),
  REJECT_NOTE_H             VARCHAR2(3000 BYTE),
  EDIT_TICKET               VARCHAR2(56 BYTE)
);

COMMENT ON TABLE TRANS_OVERCON_PAYMENT IS 'รายละเอียด Transaction ขอคืนเงิน Over Con. เป็นยอดรวมของคืนนายจ้าง หรือสมาชิก';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.TRANS_OVERCON_PAYMENT_ID IS 'รหัส Transaction รายละเอียด Over Con :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.TRANS_OVERCON_LOT_ID IS 'รหัส Lot ของ Transaction หลักที่ทำรายการ Over Con';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.SEQ_ID IS 'ลำดับการขอคืนตอน Key in';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.EMPYEE_ID IS 'รหัสสมาชิก (หากเป็นการจ่ายนายจ้าง จะไม่มีข้อมูล)';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.PAYEE_NAME IS 'ชื่อผู้รับเงิน';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.TBL_ID_PAYTYPE IS 'รหัส ประเภทการขอเงินคืน\nCAP = เช็คขีดคร่อม Account Payee Only\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.BANK_ID IS 'รหัสธนาคาร';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.ACC_NO IS 'เลขที่ บัญชี';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.AMOUNT IS 'จำนวนเงิน';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.PAYMENT_DATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.NOTE IS 'Note ต่าง ๆ';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.PAY_TO IS 'จ่ายเงินคืนให้แก่ :: EE=สมาชิก ,EY=นายจ้าง';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.IS_DELETED IS 'สถานะรายการว่าถูกลบแล้วหรือไม่ :: Y=ถูกลง,N=ยังไม่ถูกลบ';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.IS_RECORD_REJECT_H IS 'สถานะการถูก Reject  กรณีที่ ระบบบอกไม่ผ่านแต่ Manager ให้ผ่าน แต่ Head ไม่ให้ผ่าน \n:: Y = ถูก Reject ,N=ไม่ถูก Reject';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';

COMMENT ON COLUMN TRANS_OVERCON_PAYMENT.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';


CREATE TABLE TRANS_PAYMENT
(
  TRANS_PAYMENT_ID       NUMBER,
  CHQ_INFO_ID            NUMBER,
  NAV_DATE               DATE,
  PAYMENT_TYPE           VARCHAR2(3 BYTE),
  TBL_ID_PAYTYPE         VARCHAR2(50 BYTE),
  PAYMENTCREATETYPE      VARCHAR2(2 BYTE),
  ACC_NO_PAY             VARCHAR2(15 BYTE),
  BANK_ID_PAY            VARCHAR2(3 BYTE),
  PAYDATE                DATE,
  NAV_DATE_ALLOT         DATE,
  BENEFIT_NAME           VARCHAR2(1000 BYTE),
  AMOUNT                 NUMBER,
  AMOUNT_INIT            NUMBER,
  AMOUNT_DISBURSE        NUMBER,
  IS_AMOUNT_INCLUDE_FEE  VARCHAR2(1 BYTE),
  PAYMENT_FEE            VARCHAR2(2 BYTE),
  PAYMENT_STATUS         VARCHAR2(2 BYTE),
  NOTE                   VARCHAR2(500 BYTE),
  IS_CANCEL              VARCHAR2(1 BYTE),
  TBL_ID_REASON_CANCEL   VARCHAR2(50 BYTE),
  IS_PAY                 VARCHAR2(1 BYTE),
  IS_APPROVE             VARCHAR2(1 BYTE),
  APPROVE_DATETIME       DATE,
  APPROVE_USER_ID        VARCHAR2(10 BYTE),
  CREATE_DATETIME        DATE,
  CREATE_USER_ID         VARCHAR2(10 BYTE),
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE,
  EMPYER_ID              NUMBER,
  EMPYEE_ID              NUMBER,
  FUND_ID                NUMBER,
  REJECT_STEP            VARCHAR2(1 BYTE),
  REJECT_NOTE_H          VARCHAR2(3000 BYTE),
  REJECT_NOTE_Z          VARCHAR2(3000 BYTE),
  IS_BNF_RECIVE          VARCHAR2(1 BYTE),
  PAY_DATE_RECEIVE       DATE,
  IS_PAYMENT_RECORD      VARCHAR2(1 BYTE),
  APPROVE_TRAN_TYPE      VARCHAR2(4 BYTE),
  DELETE_STEP            VARCHAR2(1 BYTE),
  TRANSACTION_FEE        NUMBER
);

COMMENT ON TABLE TRANS_PAYMENT IS 'รายละเอียดของการจ่ายเงิน';

COMMENT ON COLUMN TRANS_PAYMENT.TRANS_PAYMENT_ID IS 'รหัสTransaction หลักที่ทำรายการต่าง ๆ  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_PAYMENT.CHQ_INFO_ID IS 'รหัสTransaction ข้อมูลเช็ค  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_PAYMENT.NAV_DATE IS 'ข้อมูลประจำ TD ';

COMMENT ON COLUMN TRANS_PAYMENT.PAYMENT_TYPE IS 'ประเภทรายการ :: CHQ=เช็ค,TRF=โอนเงิน';

COMMENT ON COLUMN TRANS_PAYMENT.TBL_ID_PAYTYPE IS '"รหัส ประเภทการขอเงินคืน
CAP = เช็คขีดคร่อม Account Payee Only
CCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)
CCO = เช็ค  ที่ขีดฆ่าผู้ถือ
TNF = โอนเงิน"';

COMMENT ON COLUMN TRANS_PAYMENT.PAYMENTCREATETYPE IS 'ประเภทรายการเช็คว่ามีแหล่งที่มาจากที่ใด :: KA=KA ออกเอง ,TF=รับโอนจากบลจ. อื่น ,KT=รับโอนจากกองอื่นภายใต้ บลจ KA';

COMMENT ON COLUMN TRANS_PAYMENT.ACC_NO_PAY IS 'เลขที่บัญชีจ่าย ';

COMMENT ON COLUMN TRANS_PAYMENT.BANK_ID_PAY IS 'ธนาคารบัญชีจ่าย';

COMMENT ON COLUMN TRANS_PAYMENT.PAYDATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_PAYMENT.NAV_DATE_ALLOT IS 'TD ที่จัดสรรรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.BENEFIT_NAME IS 'ชื่อผู้รับ ผลประโยชน์';

COMMENT ON COLUMN TRANS_PAYMENT.AMOUNT IS 'จำนวนเงินที่ต้องจ่าย (กรณีโอนผิดมากกว่า 1 รอบยอดเงินอาจจะลดลงถ้าสมาชิกเป็นผู้รับผิดชอบค่าโอน)';

COMMENT ON COLUMN TRANS_PAYMENT.AMOUNT_INIT IS 'จำนวนเงินที่ต้องจ่ายเริ่มต้น';

COMMENT ON COLUMN TRANS_PAYMENT.AMOUNT_DISBURSE IS 'จำนวนเงินที่เบิกจ่ายไปจากธนาคาร';

COMMENT ON COLUMN TRANS_PAYMENT.IS_AMOUNT_INCLUDE_FEE IS 'จำนวนที่ต้องการโอนรวมค่า Fee  แล้วหรือไม่ ถ้ารวมจะจ่ายสมาชิกเต็มจำนวน โดยถือว่ากองทุนเป็นผู้ออก :: Y =รวม ;N= ไม่รวม';

COMMENT ON COLUMN TRANS_PAYMENT.PAYMENT_FEE IS 'ค่าธรรมเนียมการจ่ายเงิน - กรณีจ่ายด้วยเช็ค :: MR=สมาชิกเป็นผู้รับผิดชอบ ,FR=เป็นค่าใช้จ่ายกองทุน';

COMMENT ON COLUMN TRANS_PAYMENT.PAYMENT_STATUS IS '"สถานะรายการจ่าย :: 
I=รออนุมัติรายการเปลี่ยนการจ่ายเงิน
H=Head Section Approve 
Z=ผู้บริหารอนุมัติ 
R=Reject"';

COMMENT ON COLUMN TRANS_PAYMENT.NOTE IS 'เหตุผลการยกเลิก หรือ Reject';

COMMENT ON COLUMN TRANS_PAYMENT.IS_CANCEL IS 'สถานะการยกเลิกรายการ กรณีออกรายการ Payment ใหม่ :: Y=ยกเลิก , N=ไม่ยกเลิก';

COMMENT ON COLUMN TRANS_PAYMENT.TBL_ID_REASON_CANCEL IS '"เหตุผลการยกเลิก
CHGC=เปลี่ยนแปลงการจ่ายเงิน จากโอนเป็นเช็ค 
CHGT=เปลี่ยนแปลงการจ่ายเงิน จากเช็ค เป็นโอน
CHOV = เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่"';

COMMENT ON COLUMN TRANS_PAYMENT.IS_PAY IS 'ผู้รับผลประโยชน์ได้รับเงินแล้ว (ถ้าเป็นเช็ค คือ ขึ้นเงิน ถ้าเป็นโอน คือ โอนเงินสำเร็จ) :: Y=ได้รับแล้ว ,N=ยังไม่ได้รับ';

COMMENT ON COLUMN TRANS_PAYMENT.IS_APPROVE IS 'อนุม้ติรายการจ่ายเงิน';

COMMENT ON COLUMN TRANS_PAYMENT.APPROVE_DATETIME IS 'วันที่อนุมัติรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.CREATE_USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_PAYMENT.EMPYER_ID IS 'รหัสนายจ้าง';

COMMENT ON COLUMN TRANS_PAYMENT.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN TRANS_PAYMENT.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN TRANS_PAYMENT.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN TRANS_PAYMENT.REJECT_NOTE_H IS 'สาเหตุการ Reject ของ Section Head';

COMMENT ON COLUMN TRANS_PAYMENT.REJECT_NOTE_Z IS 'สาเหตุการ Reject ของ ผู้บริหาร';

COMMENT ON COLUMN TRANS_PAYMENT.IS_BNF_RECIVE IS 'ผู้รับผลประโยชน์ได้รับเงินแล้ว (ถ้าเป็นเช็ค คือ ขึ้นเงิน ถ้าเป็นโอน คือ โอนเงินสำเร็จ) :: Y=ได้รับแล้ว ,N=ยังไม่ได้รับ';

COMMENT ON COLUMN TRANS_PAYMENT.PAY_DATE_RECEIVE IS 'วันที่สมาชิกรับเงิน กรณีเป็นเช็ค คือวันที่นำเช็คมาขึ้นเงิน จะได้ตอน Map Statement ถ้าเป็นโอนจะเป็นวันที่รับโอน';

COMMENT ON COLUMN TRANS_PAYMENT.IS_PAYMENT_RECORD IS 'สถานะว่าเป็นรายการที่ต้องนำไปใช้เพื่อการจ่ายเงินหรือไม่\nY = นำไปใช้เพื่อการ จ่ายเงิน\nN = ไม่นำไปใช้เพื่อการจ่ายเงิน เช่น \n1. กันเงินภาษี-ไม่สร้างรายการจ่าย \n2. ลาออกพ้นสมาชิกภาพแบบโอนย้ายภายใต้กองทุนเดียวกัน (subfund เดิม)\n3. ลาออกพ้นสมาชิกภาพแบบคงเงิน\n4. ลาออกพ้นสมาชิกภาพแบบโอนย้าย';

COMMENT ON COLUMN TRANS_PAYMENT.APPROVE_TRAN_TYPE IS 'ประเภทรายการ PF (0. รายการจ่ายระดับกองทุนหลัก)\nPTAX = กันเงินภาษี (ภาษีรายสมาชิกที่ลาออกแต่ละ TD)\nPFEE = ค่าธรรมเนียมเงินงวด\n\nประเภทรายการ NU (1. รายการที่ไม่ต้องจัดสรร)\nOVCN=รายการ Over Con.\nRMBP=Reaimburse\nCTAX=รายการจ่ายสรรพากร\nAPBP = ตั้งเเช็ค Over Due เช็คค้างจ่ายเกิน 6 เดือน ต้องนำส่งผู้ชำระบัญชี เบิกเงินจากบัญชีจ่ายไปบัญชีลงทุน\nCHGP=เปลี่ยนแปลงการจ่ายเงิน เช่นเปลี่ยนจากโอนเป็นเช็ค หรือเปลี่ยนเช็ค\nCHOV = เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่\n\nประเภทรายการ CM (2. รายการที่ระบบตรวจสอบพบข้อผิดพลาดแต่ Manager ให้ผ่าน)\nVTPA = รายการตรวจสอบจำนวนรายการจัดสรร  \nVEST = รายการตรวจสอบยอดเงิน Estimate \nVUNV = รายการตรวจสอบยอดเงิน Unvested\nVTAX = รายการตรวจสอบภาษีที่จ่ายให้สรรพากร\nVCNF = รายการตรวจสอบความขัดแย้ง\n\n\nประเภทรายการจ่ายเงิน (รายการตามรายงาน EOD18 ประเภทรายการ TP)\n(3. รายการที่ได้จากหลังจัดสรร )\nCLMA=ลาออกพ้นสภาพทั้งหมด\nCLMT=ลาออกพ้นสภาพแบบโอนย้าย  (เช่น PF1103 ไป PF2103) (ไม่สร้างรายการ Payment)\nCLMS=ลาออกพ้นสถาพแบบคงเงิน\nCTNF=ลาออกพ้นสภาพแบบโอนย้ายภายใต้กองทุนเดียวกัน (เช่น PF1103 ไป PF1103) (ไม่สร้างรายการ Payment)\nCAET=ลาออกพ้นสภาพแบบรับเงินงวด\nOCAL= จ่ายเงินคืนกรณีส่งเงิน Con. เงิน\nTNFO = ลาออกพ้นสภาพไป บลจ.อื่น\nTNFF = ลาออกพ้นสภาพไป กองทุนอี่นภายใต้บลจ. เดียวกัน (เช่น PF0079 ไป PF0103) (คนละนิติบุคคล)\nTRMF = โอนย้ายไป RMF\nUNVE= Unvest จ่ายคืนนายจ้าง';

COMMENT ON COLUMN TRANS_PAYMENT.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_PAYMENT.TRANSACTION_FEE IS 'จำนวนเงินค่าธรรมเนียมที่ หักออก หรือเพื่อขึ้น จ่ายรายการ Payment เช่น ค่าธรรมเนียมเงินงวด';


CREATE TABLE TRANS_REIMBURSE
(
  TRANS_REIMBURSE_ID         NUMBER,
  NAV_DATE                   DATE,
  EMPYEE_ID                  NUMBER,
  EMPYER_ID                  NUMBER,
  FUND_ID                    NUMBER,
  HAVE_STATEMENT             VARCHAR2(1 BYTE),
  INSTRUCTION_FLAG           VARCHAR2(1 BYTE),
  BANK_ID                    VARCHAR2(4 BYTE),
  ACC_NO                     VARCHAR2(15 BYTE),
  REIMBURSE_REASON           NVARCHAR2(1000),
  TRANSFER_DATE              DATE,
  STM_ID                     NUMBER,
  TOTAL_AMT                  NUMBER,
  TBL_ID_PAYTYPE             VARCHAR2(50 BYTE),
  PAYEE_NAME                 VARCHAR2(1000 BYTE),
  BANK_ID_PAY                VARCHAR2(4 BYTE),
  ACC_NO_PAY                 VARCHAR2(15 BYTE),
  AMOUNT                     NUMBER,
  PAYMENT_DATE               DATE,
  NOTE                       VARCHAR2(1000 CHAR),
  REIMBURSE_STATUS           VARCHAR2(2 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  HAVE_AMT_UNALLOCATE        VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  ACC_WORK                   VARCHAR2(2 BYTE),
  REJECT_NOTE_H              VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE TRANS_REIMBURSE IS 'Transaction การขอคืนเงินออกจากบัญชี';

COMMENT ON COLUMN TRANS_REIMBURSE.TRANS_REIMBURSE_ID IS 'รหัสTransaction หลักการขอคืนเงินแบบ Reimburse  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_REIMBURSE.NAV_DATE IS 'วันที่ trade date ที่ต้องการให้รายการไปพร้อมกัน';

COMMENT ON COLUMN TRANS_REIMBURSE.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN TRANS_REIMBURSE.EMPYER_ID IS 'รหัสนายจ้าง';

COMMENT ON COLUMN TRANS_REIMBURSE.FUND_ID IS 'รหัสกองทุน';

COMMENT ON COLUMN TRANS_REIMBURSE.HAVE_STATEMENT IS 'การขอคืนเงินจากบัญชีที่มี Statement หรือไม่ :: Y=มี ,N=ไม่มี';

COMMENT ON COLUMN TRANS_REIMBURSE.INSTRUCTION_FLAG IS 'ต้องทำ Instruction';

COMMENT ON COLUMN TRANS_REIMBURSE.BANK_ID IS 'รหัสธนาคารของเงินที่ขอคืน';

COMMENT ON COLUMN TRANS_REIMBURSE.ACC_NO IS 'เลขที่บัญชีที่ขอคืน';

COMMENT ON COLUMN TRANS_REIMBURSE.REIMBURSE_REASON IS 'สาเหตุการขอคืนเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.TRANSFER_DATE IS 'วันที่ของ Statement ที่โอนมาผิด';

COMMENT ON COLUMN TRANS_REIMBURSE.STM_ID IS 'รหัสของ Statement ที่ทำการ Import อ้างอิง Table : STATEMENT_DESC';

COMMENT ON COLUMN TRANS_REIMBURSE.TOTAL_AMT IS 'จำนวนเงินเต็มตาม Statement ณ วันที่ทำรายการ';

COMMENT ON COLUMN TRANS_REIMBURSE.TBL_ID_PAYTYPE IS 'รหัส ประเภทการขอเงินคืน\nCAP = เช็คขีดคร่อม Account Payee Only\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.PAYEE_NAME IS 'ชื่อผู้รับเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.BANK_ID_PAY IS 'รหัสธนาคารที่ขอรับเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.ACC_NO_PAY IS 'เลขที่ บัญชีที่ขอรับเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.AMOUNT IS 'จำนวนเงินที่ขอรับเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.PAYMENT_DATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_REIMBURSE.NOTE IS 'Note สาเหตุการ Reject';

COMMENT ON COLUMN TRANS_REIMBURSE.REIMBURSE_STATUS IS 'สถานะการขอคืนเงิน :: :: I=เริ่มบันทึกรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN TRANS_REIMBURSE.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: M=Manager, H=Section Head';

COMMENT ON COLUMN TRANS_REIMBURSE.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_REIMBURSE.HAVE_AMT_UNALLOCATE IS 'มีเงินเพียงพอหรือไม่   :: Y=มี, N=ไม่มี';

COMMENT ON COLUMN TRANS_REIMBURSE.APPROVE_M_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN TRANS_REIMBURSE.APPROVE_M_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN TRANS_REIMBURSE.APPROVE_H_USER_ID IS 'รหัสผู้สร้างรายการ (Head Section)';

COMMENT ON COLUMN TRANS_REIMBURSE.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN TRANS_REIMBURSE.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_REIMBURSE.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_REIMBURSE.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN TRANS_REIMBURSE.ACC_WORK IS 'วัตถุประสงค์ของบัญชี :: W=บัญชีดำเนินการ หรือบัญชีลงทุน ,P=บัญชีจ่าย,I=บัญชีกองทุน';

COMMENT ON COLUMN TRANS_REIMBURSE.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE TRANS_REIMBURSE_DOCUMENT
(
  TRANS_REIMBURSE_ID           NUMBER,
  TBL_ID_DOCLIST               VARCHAR2(50 BYTE),
  HAVE_DOC                     VARCHAR2(1 BYTE),
  USER_ID                      VARCHAR2(10 BYTE),
  UPDATE_DATETIME              DATE,
  TRANS_REIMBURSE_DOCUMENT_ID  NUMBER
);

COMMENT ON TABLE TRANS_REIMBURSE_DOCUMENT IS 'รายการเอกสารแนบการขอคืนเงินออกจากบัญชี';


CREATE TABLE TRANS_TAX
(
  TRANS_TAX_ID               NUMBER,
  FUND_ID                    NUMBER,
  NAV_DATE                   DATE,
  PAYEE_NAME                 VARCHAR2(1000 BYTE),
  TBL_ID_PAYTYPE             VARCHAR2(50 BYTE),
  BANK_ID                    VARCHAR2(4 BYTE),
  ACC_NO                     VARCHAR2(15 BYTE),
  AMOUNT                     NUMBER,
  PAYMENT_DATE               DATE,
  NOTE                       VARCHAR2(1000 BYTE),
  REJECT_STEP                VARCHAR2(1 BYTE),
  DELETE_STEP                VARCHAR2(1 BYTE),
  APPROVE_M_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_M_UPDATE_DATETIME  DATE,
  APPROVE_H_USER_ID          VARCHAR2(10 BYTE),
  APPROVE_H_UPDATE_DATETIME  DATE,
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  EDIT_TICKET                VARCHAR2(56 BYTE),
  TRANS_TAX_STATUS           VARCHAR2(2 BYTE),
  REJECT_NOTE_H              VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE TRANS_TAX IS 'รายละเอียด Transaction ภาษีเพื่อจ่ายสรรพากร';

COMMENT ON COLUMN TRANS_TAX.TRANS_TAX_ID IS 'รหัส Transaction ภาษี  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN TRANS_TAX.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN TRANS_TAX.NAV_DATE IS 'ข้อมูลประจำ TD';

COMMENT ON COLUMN TRANS_TAX.PAYEE_NAME IS 'ชื่อผู้รับเงิน (ข้อมูลชื่อจะเป็นสรรพากร)';

COMMENT ON COLUMN TRANS_TAX.TBL_ID_PAYTYPE IS 'รหัส ประเภทการขอเงินคืน\nCAP = เช็คขีดคร่อม Account Payee Only\nCCB = เช็ค  Bearer(ไม่ขีดฆ่าผู้ถือ)\nCCO = เช็ค  ที่ขีดฆ่าผู้ถือ\nTNF = โอนเงิน';

COMMENT ON COLUMN TRANS_TAX.BANK_ID IS 'รหัสธนาคาร';

COMMENT ON COLUMN TRANS_TAX.ACC_NO IS 'เลขที่ บัญชี';

COMMENT ON COLUMN TRANS_TAX.AMOUNT IS 'จำนวนเงิน';

COMMENT ON COLUMN TRANS_TAX.PAYMENT_DATE IS 'วันที่จ่ายเงิน';

COMMENT ON COLUMN TRANS_TAX.NOTE IS 'Note';

COMMENT ON COLUMN TRANS_TAX.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, M=Manager, H=Section Head';

COMMENT ON COLUMN TRANS_TAX.DELETE_STEP IS 'สถานะรายการว่าใครสามารถลบรายการได้ :: S=Staff, M=Manager,H=Head Section';

COMMENT ON COLUMN TRANS_TAX.APPROVE_M_USER_ID IS 'รหัสผู้อนุมัติรายการล่าสุด (Manager)';

COMMENT ON COLUMN TRANS_TAX.APPROVE_M_UPDATE_DATETIME IS 'วันเดือนปีผู้อนุมัติรายการล่าสุด';

COMMENT ON COLUMN TRANS_TAX.APPROVE_H_USER_ID IS 'รหัสผู้สร้างรายการ (Section Head)';

COMMENT ON COLUMN TRANS_TAX.APPROVE_H_UPDATE_DATETIME IS 'วันเดือนปีผู้สร้างรายการ';

COMMENT ON COLUMN TRANS_TAX.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN TRANS_TAX.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN TRANS_TAX.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';

COMMENT ON COLUMN TRANS_TAX.TRANS_TAX_STATUS IS 'สถานะรายการภาษีจ่ายสรรพากร :: :: I=เริ่มบันทึกรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN TRANS_TAX.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE VERIFY_APPROVE_LOT
(
  VERIFY_APPROVE_LOT_ID        NUMBER,
  FUND_ID                      NUMBER,
  OBJ_TRAN_TYPE_FROM           VARCHAR2(2 BYTE),
  APPROVE_TRAN_TYPE            VARCHAR2(4 BYTE),
  VALIDATE_TOTAL_DBS_AMOUNT    NUMBER,
  VALIDATE_TRANSFER_AMOUNT     NUMBER,
  VALIDATE_CHQ_AMOUNT          NUMBER,
  VALIDATE_CLMI_AMOUNT         NUMBER,
  USER_ID                      VARCHAR2(10 BYTE),
  UPDATE_DATETIME              DATE,
  VALIDATE_UNDISBURSED_AMOUNT  NUMBER,
  VALIDATE_FI_AMOUNT           NUMBER,
  CM_IS_CONFIRM                VARCHAR2(1 BYTE),
  CM_IS_REJECT                 VARCHAR2(1 BYTE),
  FUND_PAYMENT_APPROVE_LOT_ID  NUMBER,
  FUND_IN_ID                   NUMBER
);

COMMENT ON TABLE VERIFY_APPROVE_LOT IS 'ผลการตรวจสอบข้อมูลก่อนส่ง Final Approve';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VERIFY_APPROVE_LOT_ID IS 'เลขที่ข้อมูลการตรวจสอบรายการ Lot ของ Payment:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.FUND_ID IS 'รหัสกองทุน หรือ รหัสนโยบาย';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.OBJ_TRAN_TYPE_FROM IS 'แหล่งของประเภทรายการ :: \nTP = เป็นรายการที่ได้จากหลังจัดสรร\nNU=เป็นรายการที่ไม่ได้จัดสรร (Over Con,Reimburse,Over Due)\nCM = รายการที่ระบบตรวจสอบพบข้อผิดพลาดแต่ Manager ให้ผ่าน\nPF =รายการจ่ายเงินระดับกองทุนหลัก';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.APPROVE_TRAN_TYPE IS 'ประเภทรายการ PF (0. รายการจ่ายระดับกองทุนหลัก)\nPTAX = กันเงินภาษี (ภาษีรายสมาชิกที่ลาออกแต่ละ TD)\nPFEE = ค่าธรรมเนียมเงินงวด\n\nประเภทรายการ NU (1. รายการที่ไม่ต้องจัดสรร)\nOVCN=รายการ Over Con.\nRMBP=Reaimburse\nCTAX=รายการจ่ายสรรพากร\nAPBP = เช็ค Over Due เช็คค้างจ่ายเกิน 6 เดือน ต้องนำส่งผู้ชำระบัญชี เบิกเงินจากบัญชีจ่ายไปบัญชีลงทุน\nCHGP=เปลี่ยนแปลงการจ่ายเงิน เช่นเปลี่ยนจากโอนเป็นเช็ค หรือเปลี่ยนเช็ค\nCHOV = เช็คครบกำหนด 6 เดือน แล้วขอออกเช็คใหม่\n\nประเภทรายการ CM (2. รายการที่ระบบตรวจสอบพบข้อผิดพลาดแต่ Manager ให้ผ่าน)\nVTPA = รายการตรวจสอบจำนวนรายการจัดสรร  \nVEST = รายการตรวจสอบยอดเงิน Estimate \nVUNV = รายการตรวจสอบยอดเงิน Unvested\nVTAX = รายการตรวจสอบภาษีที่จ่ายให้สรรพากร\nVCNF = รายการตรวจสอบความขัดแย้ง\n\n\nประเภทรายการจ่ายเงิน (รายการตามรายงาน EOD18 ประเภทรายการ TP)\n(3. รายการที่ได้จากหลังจัดสรร )\nCLMA=ลาออกพ้นสภาพทั้งหมด\nCLMT=ลาออกพ้นสภาพแบบโอนย้าย  (เช่น PF1103 ไป PF2103) (ไม่สร้างรายการ Payment)\nCLMS=ลาออกพ้นสถาพแบบคงเงิน\nCTNF=ลาออกพ้นสภาพแบบโอนย้ายภายใต้กองทุนเดียวกัน (เช่น PF1103 ไป PF1103) (ไม่สร้างรายการ Payment)\nCAET=ลาออกพ้นสภาพแบบรับเงินงวด\nOCAL= จ่ายเงินคืนกรณีส่งเงิน Con. เงิน\nTNFO = ลาออกพ้นสภาพไป บลจ.อื่น\nTNFF = ลาออกพ้นสภาพไป กองทุนอี่นภายใต้บลจ. เดียวกัน (เช่น PF0079 ไป PF0103) (คนละนิติบุคคล)\nTRMF = โอนย้ายไป RMF\nUNVE= Unvest จ่ายคืนนายจ้าง';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_TOTAL_DBS_AMOUNT IS 'จำนวนเงินรวมของรายการจ่าย(โอน+เช็ค+CLMI) (ถูกตรวจสอบระดับ Head Section)';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_TRANSFER_AMOUNT IS 'จำนวนเงินการจ่ายแบบโอน (ถูกตรวจสอบระดับ Head Section)';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_CHQ_AMOUNT IS 'จำนวนเงินการจ่ายแบบเช็ค (ถูกตรวจสอบระดับ Head Section)';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_CLMI_AMOUNT IS 'จำนวนเงินโอนย้ายภายใต้ บลจ. เดียวกัน (ถูกตรวจสอบระดับ Head Section)';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_UNDISBURSED_AMOUNT IS 'เงินที่ไม่จ่ายออก';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.VALIDATE_FI_AMOUNT IS 'จำนวนเงินคงเงินไว้ในกองทุน';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.CM_IS_CONFIRM IS '''Y'' ตรวจสอบแล้ว\n''N'' ยังไม่ตรวจสอบ';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.CM_IS_REJECT IS 'รายการที่ระบบตรวจสอบพบข้อผิดพลาดแต่ Manager ให้ผ่านถูกปฏิเสธด้วย Head \nY = ถูกปฏิเสธ\nN = ไม่ถูกปฏิเสธ';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.FUND_PAYMENT_APPROVE_LOT_ID IS 'เลขที่ข้อมูลกองทุนใน Lot :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_APPROVE_LOT.FUND_IN_ID IS 'กองทุนปลายทาง (กรณีโอนย้าย)';


CREATE TABLE VERIFY_CONFLICT_ERROR_DETAIL
(
  VERIFY_CONFLICT_ERR_DETAIL_ID  NUMBER,
  VERIFY_TP_HEADER_ID            NUMBER,
  EMPYEE_ID                      NUMBER,
  TBL_ID_CONFLICT_ERROR          VARCHAR2(50 BYTE),
  DETAIL_CONFILCT_ERROR          VARCHAR2(500 BYTE),
  VERIFY_CONFLICT_IS_ERR_SYSTEM  VARCHAR2(1 BYTE),
  VERIFY_CONFLICT_IS_ERR_MANUAL  VARCHAR2(1 BYTE),
  USER_ID                        VARCHAR2(10 BYTE),
  UPDATE_DATETIME                DATE,
  PASS_NOTE_M                    NVARCHAR2(500),
  REJECT_STEP                    VARCHAR2(1 BYTE),
  IS_RECORD_REJECT_H             VARCHAR2(1 BYTE),
  REJECT_NOTE_H                  VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE VERIFY_CONFLICT_ERROR_DETAIL IS 'รายละเอียดรายการตรวจสอบความขัดแย้ง เก็บค่าเฉพาะ Error';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.VERIFY_CONFLICT_ERR_DETAIL_ID IS 'รหัสรายละเอียดการตรวจสอบรายการ Conflict ต่าง ๆ :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.TBL_ID_CONFLICT_ERROR IS 'รหัสสาเหตุของ Error :: \n1=ชื่อผู้รับผลประโยชน์ของลาออกปกติไม่ใช่ชื่อสมาชิก\n2=เลขที่บัญชีของผู้รับโอนไม่ตรงกับที่ระบุไว้ใน Profile กรณีนายจ้างระบุเลขที่บัญชี\n3=กรณีโอนย้ายไปกองทุนอื่น แต่ Profile กองทุนหายไป\n4=วันที่อายุสมาชิก อายุงาน และอายุกองทุน ต้องขัดแย้งกัน\n5=ไม่มีข้อมูลที่อยู่ของสมาชิกคงเงิน / รับเงินงวด และบัญชีเพื่อรับโอน \n6= ตรวจสอบเลขบัญชีธนาคารต้องซ้ำกับเลขที่บัญชีรายอื่น\n7=ตรวจสอบยอดเงินโอนขั้นต่ำน้อยกว่า 20 บาท';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.DETAIL_CONFILCT_ERROR IS 'รายละเอียด Error เช่น ชื่อรับผลประโยชน์=XXXXX ชื่อสมาชิก = YYYYY';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.VERIFY_CONFLICT_IS_ERR_SYSTEM IS 'ตรวจสอบรายการด้วยระบบว่ามี Conflict หรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.VERIFY_CONFLICT_IS_ERR_MANUAL IS 'ตรวจสอบรายการด้วย Manual ว่ามี Conflict หรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.PASS_NOTE_M IS 'กรณีระบบตรวจพบ Error และ User ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.IS_RECORD_REJECT_H IS 'สถานะการถูก Reject  กรณีที่ ระบบบอกไม่ผ่านแต่ Manager ให้ผ่าน แต่ Head ไม่ให้ผ่าน \n:: Y = ถูก Reject ,N=ไม่ถูก Reject';

COMMENT ON COLUMN VERIFY_CONFLICT_ERROR_DETAIL.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE VERIFY_ESTIMATE_PAY_DETAIL
(
  VERIFY_ESTIMATE_PAY_DETAIL_ID  NUMBER,
  VERIFY_TP_HEADER_ID            NUMBER,
  FUND_ID                        NUMBER,
  AMC_ID                         NUMBER,
  NAV_BF_TP                      NUMBER,
  NAV_AF_TP                      NUMBER,
  NAV_PER_UNIT_BF_TP             NUMBER,
  NAV_PER_UNIT_AF_TP             NUMBER,
  TRANSFER_IN_AMT_BF_TP          NUMBER,
  TRANSFER_IN_AMT_AF_TP          NUMBER,
  CLAIM_AMT_BF_TP                NUMBER,
  CLAIM_AMT_AF_TP                NUMBER,
  RETAIN_BALANCE_AMT_BF_TP       NUMBER,
  RETAIN_BALANCE_AMT_AF_TP       NUMBER,
  RECEIVE_INSTALLMENT_AMT_BF_TP  NUMBER,
  RECEIVE_INSTALLMENT_AMT_AF_TP  NUMBER,
  NET_AMT_BF_TP                  NUMBER,
  NET_AMT_AF_TP                  NUMBER,
  OVERRESIGN_DONATE_AMT_BF_TP    NUMBER,
  OVERRESIGN_DONATE_AMT_AF_TP    NUMBER,
  MARK_UP_AMT                    NUMBER,
  ESTIMATE_PAY_TOTAL             NUMBER,
  ACTUAL_PAY_TOTAL               NUMBER,
  ESTIMATE_IS_ERROR_SYSTEM       VARCHAR2(1 BYTE),
  ESTIMATE_IS_ERROR_MANUAL       VARCHAR2(1 BYTE),
  USER_ID                        VARCHAR2(10 BYTE),
  UPDATE_DATETIME                DATE,
  PERCENT_NAV_PER_UNIT_CHANGE    NUMBER,
  PERCENT_NAV_PER_UNIT_CEILING   NUMBER,
  PERCENT_NAV_PER_UNIT_FLOOR     NUMBER,
  PERCENT_ACTUAL_PAY_TOTAL       NUMBER,
  PASS_NOTE_M                    NVARCHAR2(500),
  REJECT_STEP                    VARCHAR2(1 BYTE),
  IS_RECORD_REJECT_H             VARCHAR2(1 BYTE),
  REJECT_NOTE_H                  VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE VERIFY_ESTIMATE_PAY_DETAIL IS 'รายละเอียดรายการตรวจสอบยอดเงิน Estimate และจำนวนเงินที่จ่ายจริงหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.VERIFY_ESTIMATE_PAY_DETAIL_ID IS 'รหัสรายละเอียดการตรวจสอบยอด Estimate:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.FUND_ID IS 'รหัส sub fund';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.AMC_ID IS 'รหัส บลจ.';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NAV_BF_TP IS 'NAV  เอามาจาก Estimate Detail';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NAV_AF_TP IS 'NAV  เอามาจาก emyee cur bal โดยที่ sts <> I  โดย sum 4 ขา';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NAV_PER_UNIT_BF_TP IS 'ราคาต่อหน่วย';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NAV_PER_UNIT_AF_TP IS 'ราคาต่อหน่วย  เอาจาก daily';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.TRANSFER_IN_AMT_BF_TP IS 'จำนวนเงินรับโอนก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.TRANSFER_IN_AMT_AF_TP IS 'จำนวนเงินรับโอนหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.CLAIM_AMT_BF_TP IS 'จำนวนเงินจ่ายพ้นสภาพก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.CLAIM_AMT_AF_TP IS 'จำนวนเงินจ่ายพ้นสภาพหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.RETAIN_BALANCE_AMT_BF_TP IS 'จำนวนเงินจ่ายคนคงเงินก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.RETAIN_BALANCE_AMT_AF_TP IS 'จำนวนเงินจ่ายคนคงเงินหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.RECEIVE_INSTALLMENT_AMT_BF_TP IS 'จำนวนเงินจ่ายคนรับเงินงวดก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.RECEIVE_INSTALLMENT_AMT_AF_TP IS 'จำนวนเงินจ่ายคนรับเงินงวดหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NET_AMT_BF_TP IS 'Net Amountก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.NET_AMT_AF_TP IS 'Net Amountหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.OVERRESIGN_DONATE_AMT_BF_TP IS 'จำนวนเงินจ่าย Over Resign และเงินบริจาคก่อนจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.OVERRESIGN_DONATE_AMT_AF_TP IS 'จำนวนเงินจ่าย Over Resign และเงินบริจาคหลังจัดสรร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.MARK_UP_AMT IS 'ยอดประมาณการ 5%';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.ESTIMATE_PAY_TOTAL IS 'ยอดเงินประมาณจ่ายที่ส่งบัญชี';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.ACTUAL_PAY_TOTAL IS 'ยอดเงินที่จ่ายจริง';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.ESTIMATE_IS_ERROR_SYSTEM IS 'ตรวจสอบยอดประมาณการจ่ายเงินด้วยระบบว่าผ่านหรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.ESTIMATE_IS_ERROR_MANUAL IS 'ตรวจสอบยอดประมาณการจ่ายเงินด้วย Manual ว่าผ่านหรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.PERCENT_NAV_PER_UNIT_CHANGE IS '% ของ Unit Price ที่เปลี่ยนแปลง';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.PERCENT_NAV_PER_UNIT_CEILING IS '% ของ Unit Price ที่เปลี่ยนแปลงได้สูงสุด';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.PERCENT_NAV_PER_UNIT_FLOOR IS '% ของ Unit Price ที่เปลี่ยนแปลงได้ต่ำสุด';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.PERCENT_ACTUAL_PAY_TOTAL IS '% ของผลต่างที่จ่ายเงินจริง';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.PASS_NOTE_M IS 'กรณีระบบตรวจพบ Error แต่ Manager ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.IS_RECORD_REJECT_H IS 'สถานะการถูก Reject  กรณีที่ ระบบบอกไม่ผ่านแต่ Manager ให้ผ่าน แต่ Head ไม่ให้ผ่าน \n:: Y = ถูก Reject ,N=ไม่ถูก Reject';

COMMENT ON COLUMN VERIFY_ESTIMATE_PAY_DETAIL.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE VERIFY_LOT_MAP_PAYMENT
(
  VERIFY_APPROVE_LOT_ID  NUMBER,
  MATCH_TRANS_ID         NUMBER,
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE
);

COMMENT ON TABLE VERIFY_LOT_MAP_PAYMENT IS 'ผลการตรวจสอบข้อมูลของ Head Section เพื่อ Map กับรายการ Match Payment เพื่อให้รู้ประเภทของรายการตอนทำตรวจสอบ';

COMMENT ON COLUMN VERIFY_LOT_MAP_PAYMENT.VERIFY_APPROVE_LOT_ID IS 'เลขที่ข้อมูลการตรวจสอบรายการ Lot ของ Payment:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_LOT_MAP_PAYMENT.MATCH_TRANS_ID IS 'รหัสการ Match รายการจ่าย เพื่อออกรายการ Payment กรณี อนุมัติผ่าน:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_LOT_MAP_PAYMENT.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_LOT_MAP_PAYMENT.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE VERIFY_RECAL_TRANS_TAX_ERROR
(
  TAX_RECALTRANSERR_ID   NUMBER,
  VERIFY_TP_HEADER_ID    NUMBER,
  EMPYEE_ID              NUMBER,
  TRANS_TAX_ID           NUMBER,
  TRAN_TAX_AMT           NUMBER,
  TAX_RECAL              NUMBER,
  TBL_ID_TERMINATE_MAIN  VARCHAR2(50 BYTE),
  TBL_ID_TERMINATE_SUB   VARCHAR2(50 BYTE),
  WORKINGYEAR_OLD        NUMBER,
  WORKINGDAY_OLD         NUMBER,
  MEMBERYEAR_OLD         NUMBER,
  MEMBERDAY_OLD          NUMBER,
  BEGINWORKINGDATE       DATE,
  ENDWORKINGDATE         DATE,
  WORKINGYEAR            NUMBER,
  WORKINGDAY             NUMBER,
  BEGINMEMBERDATE        DATE,
  ENDMEMBERDATE          DATE,
  MEMBERYEAR             NUMBER,
  MEMBERDAY              NUMBER,
  USER_ID                VARCHAR2(10 BYTE),
  UPDATE_DATETIME        DATE
);

COMMENT ON TABLE VERIFY_RECAL_TRANS_TAX_ERROR IS 'รายละเอียด Error สมาชิกที่คำนวณภาษีไม่ตรงกับยอดที่จ่ายสรรพากร แบบคำนวณใหมี \nเก็บเฉพาะรายการ Error (ตรวจและแสดงเฉพาะ TD ที่ต้องจ่ายสรรพากร) (Recal vs Trans_Tax)';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TAX_RECALTRANSERR_ID IS 'รหัสรายละเอียด Error การตรวจสอบรายการ Tax ระหว่าง Recal vs Trans_Tax :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TRANS_TAX_ID IS 'รายการภาษีที่อยู่ใน Empyee_Tax ที่นำมาใช้เปรียบเทียบ';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TRAN_TAX_AMT IS 'จำนวนเงินภาษีที่อยุ่ใน Empyee_Tax';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TAX_RECAL IS 'จำนวนเงินภาษีที่คำนวณใหม่จากคนลาออกเพื่อเปรียบเทียบกับที่จะจ่ายให้สรรพากร';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TBL_ID_TERMINATE_MAIN IS 'รหัสเหตุหลักการลาออก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.TBL_ID_TERMINATE_SUB IS 'รหัสเหตุประกอบการลาออก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.WORKINGYEAR_OLD IS 'จำนวนปีอายุงานนายจ้างเดิม';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.WORKINGDAY_OLD IS 'จำนวนวันอายุงานนายจ้างเดิม';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.MEMBERYEAR_OLD IS 'จำนวนปีอายุสมาชิกกองทุนเดิม';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.MEMBERDAY_OLD IS 'จำนวนวันอายุสมาชิกกองทุนเดิม';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.BEGINWORKINGDATE IS 'วันเริ่มงาน';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.ENDWORKINGDATE IS 'วันทำงานวันสุดท้าย';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.WORKINGYEAR IS 'จำนวนปีที่ทำงาน';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.WORKINGDAY IS 'จำนวนวันที่ทำงาน';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.BEGINMEMBERDATE IS 'วันที่เริ่มเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.ENDMEMBERDATE IS 'วันที่สิ้นสุดการเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.MEMBERYEAR IS 'จำนวนปีทีเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.MEMBERDAY IS 'จำนวนวันที่เป็นสมาชิก';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_RECAL_TRANS_TAX_ERROR.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE VERIFY_TAX
(
  VERIFY_TAX_ID               NUMBER,
  VERIFY_TP_HEADER_ID         NUMBER,
  FUND_ID                     NUMBER,
  ACC_TAX_AMOUNT              NUMBER,
  TRAN_TAX_AMOUNT             NUMBER,
  USER_ID                     VARCHAR2(10 BYTE),
  UPDATE_DATETIME             DATE,
  VERIFY_TAX_IS_ERROR_SYSTEM  VARCHAR2(1 BYTE),
  VERIFY_TAX_IS_ERROR_MANUAL  VARCHAR2(1 BYTE),
  FILETAX_FOR_VERIFY_ID       NUMBER,
  PASS_NOTE_M                 NVARCHAR2(500),
  REJECT_STEP                 VARCHAR2(1 BYTE),
  IS_RECORD_REJECT_H          VARCHAR2(1 BYTE),
  REJECT_NOTE_H               VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE VERIFY_TAX IS 'รายละเอียดรายการตรวจสอบภาษีที่จ่ายให้สรรพากร กับไฟล์ของบัญชี (ตรวจและแสดงเฉพาะ TD ที่ต้องจ่ายสรรพากร)';

COMMENT ON COLUMN VERIFY_TAX.VERIFY_TAX_ID IS 'รหัสรายละเอียดการตรวจสอบรายการ Tax :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TAX.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_TAX.FUND_ID IS 'รหัสนโยบาย กรณี กอง Single จะเป็นกอง SUB';

COMMENT ON COLUMN VERIFY_TAX.ACC_TAX_AMOUNT IS 'จำนวนเงินภาษีในแต่ละกองที่ได้จากบัญชี';

COMMENT ON COLUMN VERIFY_TAX.TRAN_TAX_AMOUNT IS 'จำนวนเงินจากรายการภาษี';

COMMENT ON COLUMN VERIFY_TAX.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_TAX.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_TAX.VERIFY_TAX_IS_ERROR_SYSTEM IS 'ตรวจสอบรายการด้วยระบบว่ามี Conflict หรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_TAX.VERIFY_TAX_IS_ERROR_MANUAL IS 'ตรวจสอบรายการด้วย Manual ว่ามี Conflict หรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_TAX.FILETAX_FOR_VERIFY_ID IS 'รหัสของ Text File ภาษี ที่ได้จาก บัญชี  :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TAX.PASS_NOTE_M IS 'กรณีระบบตรวจพบ Error และ User ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_TAX.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN VERIFY_TAX.IS_RECORD_REJECT_H IS 'สถานะการถูก Reject  กรณีที่ ระบบบอกไม่ผ่านแต่ Manager ให้ผ่าน แต่ Head ไม่ให้ผ่าน \n:: Y = ถูก Reject ,N=ไม่ถูก Reject';

COMMENT ON COLUMN VERIFY_TAX.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE VERIFY_TAX_RECAL_ERROR
(
  VERIFY_TAX_RECALERR_ID  NUMBER,
  VERIFY_TP_HEADER_ID     NUMBER,
  EMPYEE_ID               NUMBER,
  TRANS_MAIN_ID           NUMBER,
  TRAN_TAX_AMT            NUMBER,
  TAX_RECAL               NUMBER,
  TBL_ID_TERMINATE_MAIN   VARCHAR2(50 BYTE),
  TBL_ID_TERMINATE_SUB    VARCHAR2(50 BYTE),
  WORKINGYEAR_OLD         NUMBER,
  WORKINGDAY_OLD          NUMBER,
  MEMBERYEAR_OLD          NUMBER,
  MEMBERDAY_OLD           NUMBER,
  BEGINWORKINGDATE        DATE,
  ENDWORKINGDATE          DATE,
  WORKINGYEAR             NUMBER,
  WORKINGDAY              NUMBER,
  BEGINMEMBERDATE         DATE,
  ENDMEMBERDATE           DATE,
  MEMBERYEAR              NUMBER,
  MEMBERDAY               NUMBER,
  USER_ID                 VARCHAR2(10 BYTE),
  UPDATE_DATETIME         DATE
);

COMMENT ON TABLE VERIFY_TAX_RECAL_ERROR IS 'รายละเอียด Error สมาชิกที่คำนวณภาษีไม่ตรงกับที่คำนวณใหม่ \nเก็บเฉพาะรายการ Error (Recal vs Empyee_Tax)';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.VERIFY_TAX_RECALERR_ID IS 'รหัสรายละเอียด Error การตรวจสอบรายการ Tax ระหว่าง Recal vs Empyee_Tax :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.TRANS_MAIN_ID IS 'รายการสมาชิกลาออก (Transaction_main_claim)';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.TRAN_TAX_AMT IS 'จำนวนเงินภาษีที่อยุ่ใน Empyee_Tax';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.TAX_RECAL IS 'จำนวนเงินภาษีที่คำนวณใหม่จากคนลาออกเพื่อเปรียบเทียบกับที่จะจ่ายให้สรรพากร';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.TBL_ID_TERMINATE_MAIN IS 'รหัสเหตุหลักการลาออก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.TBL_ID_TERMINATE_SUB IS 'รหัสเหตุประกอบการลาออก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.WORKINGYEAR_OLD IS 'จำนวนปีอายุงานนายจ้างเดิม';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.WORKINGDAY_OLD IS 'จำนวนวันอายุงานนายจ้างเดิม';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.MEMBERYEAR_OLD IS 'จำนวนปีอายุสมาชิกกองทุนเดิม';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.MEMBERDAY_OLD IS 'จำนวนวันอายุสมาชิกกองทุนเดิม';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.BEGINWORKINGDATE IS 'วันเริ่มงาน';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.ENDWORKINGDATE IS 'วันทำงานวันสุดท้าย';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.WORKINGYEAR IS 'จำนวนปีที่ทำงาน';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.WORKINGDAY IS 'จำนวนวันที่ทำงาน';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.BEGINMEMBERDATE IS 'วันที่เริ่มเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.ENDMEMBERDATE IS 'วันที่สิ้นสุดการเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.MEMBERYEAR IS 'จำนวนปีทีเป็นสมาชิก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.MEMBERDAY IS 'จำนวนวันที่เป็นสมาชิก';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_TAX_RECAL_ERROR.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE VERIFY_TP_DETAIL
(
  VERIFY_TP_DETAIL_ID         NUMBER,
  VERIFY_TP_HEADER_ID         NUMBER,
  SUB_OBJECT_TRANS_TYPE       VARCHAR2(10 BYTE),
  EMPYER_ID                   NUMBER,
  TRANSACTION_NUM             NUMBER,
  TRANSACTION_AFTER_TP_NUM    NUMBER,
  TRANSACTION_IS_PASS_SYSTEM  VARCHAR2(1 BYTE),
  TRANSACTION_IS_PASS_MANUAL  VARCHAR2(1 BYTE),
  NOTE                        NVARCHAR2(500),
  USER_ID                     VARCHAR2(10 BYTE),
  UPDATE_DATETIME             DATE,
  PASS_NOTE_M                 NVARCHAR2(500),
  REJECT_STEP                 VARCHAR2(1 BYTE),
  IS_RECORD_REJECT_H          VARCHAR2(1 BYTE),
  REJECT_NOTE_H               VARCHAR2(3000 BYTE)
);

COMMENT ON TABLE VERIFY_TP_DETAIL IS 'รายละเอียดรายการหลังจัดสรร Error';

COMMENT ON COLUMN VERIFY_TP_DETAIL.VERIFY_TP_DETAIL_ID IS 'รหัสรายละเอียดการตรวจสอบ TP :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TP_DETAIL.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_TP_DETAIL.SUB_OBJECT_TRANS_TYPE IS 'รายการที่ได้หลังการจัดสรร (ข้อมูลในตาราง Desc_pay_claim) ประกอบด้วย\n   1. CLM = ลาออก (CLM,CLMO,CLM2)\n   2. LIKE OVRS = Over Resign\n   3. EYP = จ่ายคืนเงินงวด';

COMMENT ON COLUMN VERIFY_TP_DETAIL.EMPYER_ID IS 'รหัสนายจ้าง';

COMMENT ON COLUMN VERIFY_TP_DETAIL.TRANSACTION_NUM IS 'จำนวนรายการก่อนประมวลผล';

COMMENT ON COLUMN VERIFY_TP_DETAIL.TRANSACTION_AFTER_TP_NUM IS 'จำนวนรายการหลังประมวลผล';

COMMENT ON COLUMN VERIFY_TP_DETAIL.TRANSACTION_IS_PASS_SYSTEM IS 'ตรวจสอบจำนวนรายการหลังจัดสรรว่าผ่านหรือไม่ จากที่ระบบตรวจ:: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_TP_DETAIL.TRANSACTION_IS_PASS_MANUAL IS 'ตรวจสอบจำนวนรายการหลังจัดสรรว่าผ่านหรือไม่ จากการตรวจแบบ Manual :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_TP_DETAIL.NOTE IS 'กรณีระบบตรวจพบ Error และ User ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_TP_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_TP_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_TP_DETAIL.PASS_NOTE_M IS 'กรณีระบบตรวจพบ Error แต่ Manager ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_TP_DETAIL.REJECT_STEP IS 'รายการ Reject เกิดขึ้นที่ขั้นตอนใด :: "" = N/A, H=Section Head,Z=ผู้บริหาร';

COMMENT ON COLUMN VERIFY_TP_DETAIL.IS_RECORD_REJECT_H IS 'สถานะการถูก Reject  กรณีที่ ระบบบอกไม่ผ่านแต่ Manager ให้ผ่าน แต่ Head ไม่ให้ผ่าน \n:: Y = ถูก Reject ,N=ไม่ถูก Reject';

COMMENT ON COLUMN VERIFY_TP_DETAIL.REJECT_NOTE_H IS 'สาเหตุการ Reject จาก Head Section';


CREATE TABLE VERIFY_TP_DETAIL_ERROR
(
  VERIFY_TP_DETAIL_ERR_ID  NUMBER,
  VERIFY_TP_DETAIL_ID      NUMBER,
  REQ_AMT                  NUMBER,
  TRANS_MAIN_ID            NUMBER,
  TRANS_TYPE_ERR           VARCHAR2(3 BYTE),
  EMPYEE_ID                NUMBER,
  USER_ID                  VARCHAR2(10 BYTE),
  UPDATE_DATETIME          DATE
);

COMMENT ON TABLE VERIFY_TP_DETAIL_ERROR IS 'รายละเอียดรายการหลังจัดสรร Error กรณีจำนวนรายการไม่เท่ากันจะแสดงว่า error ที่รายการใด (เก็บเฉพาะ Error)';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.VERIFY_TP_DETAIL_ERR_ID IS 'รหัสรายละเอียดการตรวจสอบ TP ERROR :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.VERIFY_TP_DETAIL_ID IS 'รหัสรายละเอียดการตรวจสอบ TP :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.REQ_AMT IS 'จำนวนเงินที่ขอกรณีรับเงินงวด';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.TRANS_MAIN_ID IS 'รหัสรายการที่ Error';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.TRANS_TYPE_ERR IS 'ประเภทรายการที่ Error :: BFA=รายการก่อนจัดสรร ,AFA = รายการหลังจัดสรร';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_TP_DETAIL_ERROR.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';


CREATE TABLE VERIFY_TP_HEADER
(
  VERIFY_TP_HEADER_ID      NUMBER,
  ESTIMATE_PAY_ID          NUMBER,
  FUND_ID                  NUMBER,
  NAV_DATE                 DATE,
  TRANSACTION_IS_PASS      VARCHAR2(1 BYTE),
  ESTIMATE_PAY_IS_PASS     VARCHAR2(1 BYTE),
  UNVESTED_IS_PASS         VARCHAR2(1 BYTE),
  CONFLICT_IS_PASS         VARCHAR2(1 BYTE),
  VERIFY_TP_HEADER_STATUS  VARCHAR2(1 BYTE),
  REJECT_NOTE              VARCHAR2(500 BYTE),
  APPROVE_DATETIME         DATE,
  APPROVE_USER_ID          VARCHAR2(10 BYTE),
  CREATE_DATETIME          DATE,
  USER_ID                  VARCHAR2(10 BYTE),
  UPDATE_DATETIME          DATE,
  TAX_IS_PASS              NVARCHAR2(1),
  TRAN_TAX_AMT             NUMBER,
  TAX_RECAL                NUMBER,
  TRANSACTION_PROCESS_ID   VARCHAR2(300 BYTE),
  ESTIMATE_PAY_PROCESS_ID  VARCHAR2(300 BYTE),
  UNVESTED_PROCESS_ID      VARCHAR2(300 BYTE),
  CONFLICT_PROCESS_ID      VARCHAR2(300 BYTE),
  TAX_PROCESS_ID           VARCHAR2(300 BYTE),
  EDIT_TICKET              VARCHAR2(56 BYTE)
);

COMMENT ON TABLE VERIFY_TP_HEADER IS 'รายการตรวจสอบข้อมูลการจ่ายเงินหลังจัดสรร';

COMMENT ON COLUMN VERIFY_TP_HEADER.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_TP_HEADER.ESTIMATE_PAY_ID IS 'รหัสการทำ Estimate ส่งยอดเงินบัญชี :: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_TP_HEADER.FUND_ID IS 'รหัสกองทุนหลัก';

COMMENT ON COLUMN VERIFY_TP_HEADER.NAV_DATE IS 'TD ที่จัดสรร';

COMMENT ON COLUMN VERIFY_TP_HEADER.TRANSACTION_IS_PASS IS 'ตรวจสอบจำนวนรายการหลังจัดสรรว่าผ่านหรือไม่\n :: \nW = รอประมวลผล, \nP = กำลังประมวลผล, \nE = Error, \nF = ประมวลเสร็จ,\nA = Accurate (    ถูกต้องโดยระบบ),\nV = Violate (ไม่ถูกต้องโดยระบบ),\nY=ผ่านโดย User ,\nN=ไม่ผ่าน User,\nR=Reject จาก Head';

COMMENT ON COLUMN VERIFY_TP_HEADER.ESTIMATE_PAY_IS_PASS IS 'ตรวจสอบยอดประมาณการ การจ่ายเงินว่าผ่านหรือไม่\n :: W = รอประมวลผล, \nP = กำลังประมวลผล, \nE = Error, \nF = ประมวลเสร็จ,\nA = Accurate (    ถูกต้องโดยระบบ),\nV = Violate (ไม่ถูกต้องโดยระบบ),\nY=ผ่านโดย User ,\nN=ไม่ผ่าน User,\nR=Reject จาก Head';

COMMENT ON COLUMN VERIFY_TP_HEADER.UNVESTED_IS_PASS IS 'ตรวจสอบ Unvested ว่า ของแต่ละนายจ้างเท่ากับ เงินกระจายหรือเงินคืนนายจ้างหรือไม่ \n:: W = รอประมวลผล, \nP = กำลังประมวลผล, \nE = Error, \nF = ประมวลเสร็จ,\nA = Accurate (    ถูกต้องโดยระบบ),\nV = Violate (ไม่ถูกต้องโดยระบบ),\nY=ผ่านโดย User ,\nN=ไม่ผ่าน User,\nR=Reject จาก Head';

COMMENT ON COLUMN VERIFY_TP_HEADER.CONFLICT_IS_PASS IS 'ตรวจสอบรายการแย้งต่าง ๆ ว่าผ่าน หรือไม่ :: W = รอประมวลผล, \nP = กำลังประมวลผล, \nE = Error, \nF = ประมวลเสร็จ,\nA = Accurate (    ถูกต้องโดยระบบ),\nV = Violate (ไม่ถูกต้องโดยระบบ),\nY=ผ่านโดย User ,\nN=ไม่ผ่าน User,\nR=Reject จาก Head';

COMMENT ON COLUMN VERIFY_TP_HEADER.VERIFY_TP_HEADER_STATUS IS 'สถานะ  :: W = รอประมวลผล, P = กำลังประมวลผล, E=Error , F = ประมวลเสร็จ I=ยืนยันตรวจรายการ, M=Manager อนุมัติ, H=Section Head อนุมัติ, R=Reject, X=Delete';

COMMENT ON COLUMN VERIFY_TP_HEADER.REJECT_NOTE IS 'สาเหตุการ Reject';

COMMENT ON COLUMN VERIFY_TP_HEADER.APPROVE_DATETIME IS 'วันที่อนุมัติรายการ';

COMMENT ON COLUMN VERIFY_TP_HEADER.APPROVE_USER_ID IS 'รหัสผู้ อนุมัติรายการ';

COMMENT ON COLUMN VERIFY_TP_HEADER.CREATE_DATETIME IS 'วันที่ทำรายการ';

COMMENT ON COLUMN VERIFY_TP_HEADER.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_TP_HEADER.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_TP_HEADER.TAX_IS_PASS IS 'ตรวจสอบรายการภาษีว่าผ่านหรือไม่ :: W = รอประมวลผล, \nP = กำลังประมวลผล, \nE = Error, \nF = ประมวลเสร็จ,\nA = Accurate (    ถูกต้องโดยระบบ),\nV = Violate (ไม่ถูกต้องโดยระบบ),\nY=ผ่านโดย User ,\nN=ไม่ผ่าน User,\nR=Reject จาก Head';

COMMENT ON COLUMN VERIFY_TP_HEADER.TRAN_TAX_AMT IS 'จำนวนเงินภาษีที่อยุ่ใน Empyee_Tax (ยอดรวมทั้งกองทุน)';

COMMENT ON COLUMN VERIFY_TP_HEADER.TAX_RECAL IS 'จำนวนเงินภาษีที่คำนวณใหม่จากคนลาออกเพื่อเปรียบเทียบกับที่จะจ่ายให้สรรพากร';

COMMENT ON COLUMN VERIFY_TP_HEADER.EDIT_TICKET IS 'เลขที่การ update ใช้เพื่อป้องกันการ update ซ้ำ';


CREATE TABLE VERIFY_TP_PROGRESS
(
  VERIFY_TP_HEADER_ID      NUMBER,
  VERIFY_TP_PROCESS_ID     NUMBER,
  TRANSACTION_TP_PERCENT   INTEGER,
  ESTIMATE_PAY_TP_PERCENT  INTEGER,
  UNVESTED_TP_PERCENT      INTEGER,
  CONFLICT_TP_PERCENT      INTEGER,
  TAX_TP_PERCENT           INTEGER,
  TRANSACTION_TP_JOB_ID    VARCHAR2(20 BYTE),
  ESTIMATE_PAY_TP_JOB_ID   VARCHAR2(20 BYTE),
  UNVESTED_TP_JOB_ID       VARCHAR2(20 BYTE),
  CONFLICT_TP_JOB_ID       VARCHAR2(20 BYTE),
  TAX_TP_JOB_ID            VARCHAR2(20 BYTE)
);

CREATE TABLE VERIFY_UNVESTED_DETAIL
(
  VERIFY_UNVESTED_DETAIL_ID  NUMBER,
  VERIFY_TP_HEADER_ID        NUMBER,
  EMPYEE_ID                  NUMBER,
  UNVESTED_AMT               NUMBER,
  UNVESTED_IS_ERROR_SYSTEM   VARCHAR2(1 BYTE),
  UNVESTED_IS_ERROR_MANUAL   VARCHAR2(1 BYTE),
  NOTE                       VARCHAR2(500 BYTE),
  USER_ID                    VARCHAR2(10 BYTE),
  UPDATE_DATETIME            DATE,
  AMC_ID                     NUMBER,
  FUND_ID                    NUMBER,
  TBL_ID_UNVESTEDTO          VARCHAR2(50 BYTE),
  RETURN_UNVESTED_AMT        NUMBER,
  TBL_ID_MONEYTYPE           VARCHAR2(50 BYTE)
);

COMMENT ON TABLE VERIFY_UNVESTED_DETAIL IS 'รายละเอียดรายการตรวจสอบยอดเงิน Unvested';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.VERIFY_UNVESTED_DETAIL_ID IS 'รหัสรายละเอียดการตรวจสอบยอด Unvested :: [ปี ค.ศ.][Running] เช่น 20191';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.VERIFY_TP_HEADER_ID IS 'รหัสรายการตรวจสอบ TP:: [ปี ค.ศ.][Running] เช่น 20141';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.EMPYEE_ID IS 'รหัสสมาชิก';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.UNVESTED_AMT IS 'จำนวนเงิน Unvest';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.UNVESTED_IS_ERROR_SYSTEM IS 'ตรวจสอบยอด Unvested ด้วยระบบผ่านหรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.UNVESTED_IS_ERROR_MANUAL IS 'ตรวจสอบยอด Unvested ด้วย Manual ว่าผ่านหรือไม่ :: Y=ผ่าน ,N=ไม่ผ่าน';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.NOTE IS 'กรณีระบบตรวจพบ Error แต่ Manager ให้ผ่านต้องระบุ เหตุผล';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.USER_ID IS 'รหัสผู้ทำรายการ';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.UPDATE_DATETIME IS 'วันเดือนปี เพิ่ม/แก้ไขรายการ';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.AMC_ID IS 'รหัส บลจ.';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.FUND_ID IS 'รหัสกองทุน';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.TBL_ID_UNVESTEDTO IS 'ประเภทเงิน Unvest ที่จะต้องจ่ายคืน :: RE=จ่ายคืนนายจ้างนายจ้าง , RI=คืนนายจ้างที่จะนำไปลงทุนต่อ ,RM = คืนสมาชิกที่อยุ่ในกองทุน';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.RETURN_UNVESTED_AMT IS 'จำนวนเงิน Unvest จ่ายคืน';

COMMENT ON COLUMN VERIFY_UNVESTED_DETAIL.TBL_ID_MONEYTYPE IS 'ประเภทเงิน :: 01=เงินนำส่ง ,02=เงินประเดิม,03=เงินรับกระจาย,04=เงินสมทบพิเศษ,05=เงินก่อนการแก้ไขข้อบังคับ';

EXIT;