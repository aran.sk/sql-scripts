-- BEGIN SCRIPT --
CREATE OR REPLACE FUNCTION F_GET_TRANS_FEE_HEADER_CHECK_DE (
    p_TRANS_FEE_HEADER_ID in number
)
return TRANS_FEE_CHECK_DE_T_TABLE as
    v_trans_fee_check_detail   TRANS_FEE_CHECK_DE_T_TABLE;
    v_FUND_ID number;
    v_NAV_DATE date;
    v_EMPYEE_LIST_ID varchar2(32767);
    v_EMPYEE_LIST2_ID varchar2(32767);
begin

    SELECT FUND_ID,NAV_DATE
    INTO v_FUND_ID,v_NAV_DATE
    FROM TRANS_FEE_HEADER
    WHERE trans_fee_header_id = p_TRANS_FEE_HEADER_ID AND ROWNUM = 1;

    --Find EMPYEE_ID in TRANS_FEE_DETAIL by trans_fee_header_id
    SELECT listagg(EMPYEE_ID,',') within group (order by EMPYEE_ID) AS EMPYEE_ID
    INTO v_EMPYEE_LIST_ID
    FROM
    (
        SELECT DISTINCT EMPYEE_ID 
        FROM TRANS_FEE_HEADER TF
        INNER JOIN TRANS_FEE_DETAIL TFD ON TFD.trans_fee_header_id = TF.trans_fee_header_id
        WHERE tfd.trans_fee_header_id = p_TRANS_FEE_HEADER_ID
    );

    --Find EMPYEE_ID in Desc_Pay_Claim by FUND_ID and NAV_DATE
    SELECT listagg(EMPYEE_ID,',') within group (order by EMPYEE_ID) AS EMPYEE_ID
    INTO v_EMPYEE_LIST2_ID
    FROM
    (
        SELECT DISTINCT dp.EMPYEE_ID 
        FROM Desc_Pay_Claim dp
        INNER JOIN INSTALLMENT_TRANSACTION it ON it.INSTALL_TRAN_ID = dp.TRAN_MAIN_ID
        WHERE dp.JOB_ALL_ID = 'EYP' AND it.period_no > 0 AND dp.FUND_ID IN(SELECT FUND_ID FROM FUND WHERE FUND_ID = v_FUND_ID OR MASTER_FUND_ID = v_FUND_ID) AND dp.NAV_DATE = v_NAV_DATE
    );

    select 
      TRANS_FEE_CHECK_DE_T_RECORD
      (
      FUND_ID,
      NAV_DATE,
      EMPYER_NO,
      EMPYER_NAME,
      EMPYEE_ID,
      EMPYEE_CODE,
      EMPYEE_NAME,
      ERROR_SOURCE
      )
    bulk collect into
      v_trans_fee_check_detail
        FROM 
        (
        SELECT
        dp.FUND_ID,
        dp.NAV_DATE,
        MIN(c.empyer_no) AS EMPYER_NO,
        MIN(c.empname_thai) AS EMPYER_NAME,
        dp.EMPYEE_ID,
        MIN(E.EMPYEE_CODE) AS EMPYEE_CODE,
        MIN(TLE.TBL_DESC || e.empyee_name || ' ' || e.empyee_surname) AS EMPYEE_NAME,
        MIN('��ѧ�Ѵ���') AS ERROR_SOURCE
        FROM Desc_Pay_Claim dp
        INNER JOIN INSTALLMENT_TRANSACTION it ON it.INSTALL_TRAN_ID = dp.TRAN_MAIN_ID
        INNER JOIN EMPLOYEE E ON E.EMPYEE_ID = dp.EMPYEE_ID
        INNER JOIN TABLE_LINE TLE ON TLE.TBL_ID = e.empyee_tbl_id_titlename
        INNER JOIN EMPLOYER C ON C.EMPYER_ID = E.EMPYER_ID
        WHERE dp.JOB_ALL_ID = 'EYP' AND it.period_no > 0 AND dp.FUND_ID IN(SELECT FUND_ID FROM FUND WHERE FUND_ID = v_FUND_ID OR MASTER_FUND_ID = v_FUND_ID) AND dp.NAV_DATE = v_NAV_DATE
        AND dp.EMPYEE_ID NOT IN(
        select regexp_substr(v_EMPYEE_LIST_ID,'[^,]+', 1, level) from dual
        connect by regexp_substr(v_EMPYEE_LIST_ID, '[^,]+', 1, level) is not null
        )
        GROUP BY dp.FUND_ID,dp.EMPYEE_ID,dp.NAV_DATE
        UNION ALL
        SELECT
        MIN(TF.FUND_ID),
        MIN(TF.NAV_DATE),
        MIN(c.empyer_no) AS EMPYER_NO,
        MIN(c.empname_thai) AS EMPYER_NAME,
        TFD.EMPYEE_ID,
        MIN(E.EMPYEE_CODE) AS EMPYEE_CODE,
        MIN(TLE.TBL_DESC || e.empyee_name || ' ' || e.empyee_surname) AS EMPYEE_NAME,
        MIN('��Ҹ�������') AS ERROR_SOURCE
        FROM TRANS_FEE_DETAIL TFD
        INNER JOIN TRANS_FEE_HEADER TF ON TF.trans_fee_header_id = TFD.trans_fee_header_id
        INNER JOIN EMPLOYEE E ON E.EMPYEE_ID = TFD.EMPYEE_ID
        INNER JOIN TABLE_LINE TLE ON TLE.TBL_ID = e.empyee_tbl_id_titlename
        INNER JOIN EMPLOYER C ON C.EMPYER_ID = E.EMPYER_ID
        WHERE tfd.trans_fee_header_id = 202022
        AND tfd.EMPYEE_ID NOT IN(
         select regexp_substr(v_EMPYEE_LIST2_ID,'[^,]+', 1, level) from dual
        connect by regexp_substr(v_EMPYEE_LIST2_ID, '[^,]+', 1, level) is not null
        )
        GROUP BY TFD.EMPYEE_ID
    );

    return v_trans_fee_check_detail;

end F_GET_TRANS_FEE_HEADER_CHECK_DE;

/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION F_GET_TRANS_FEE_PAY (
    p_fund_id in number,
    p_navdate in date
)
return TRANS_FEE_PAY_T_TABLE as
    v_trans_fee_pay   TRANS_FEE_PAY_T_TABLE;
    PAYEE_NAME varchar2(1000);
    PAY_TYPE varchar2(5);
    BANK_ID varchar2(5);
    ACC_NO varchar2(15);
    PAYMENT_DATE date;
    TRANSACTION_FEE number;
    v_HOLIDAY_DATE date;
    v_count number:=0;
    v_loop number:= 0;
begin

    SELECT TBL_VALUE INTO PAYEE_NAME FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'PAYEE_NAME';
    SELECT TBL_VALUE INTO PAY_TYPE FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'PAY_TYPE';
    SELECT TBL_VALUE INTO BANK_ID FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'BANK_ID';
    SELECT TBL_VALUE INTO ACC_NO FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'ACC_NO';
    SELECT TBL_VALUE INTO TRANSACTION_FEE FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'TRANSACTION_FEE';

    Select (p_navdate + payment_deal_day)
    INTO PAYMENT_DATE
    From regu_claim 
    Where fund_id = p_fund_id AND rownum = 1
    ORDER BY effective_date DESC;

    while v_loop = 0 loop

        SELECT COUNT(HOLIDAY_DATE)
        INTO v_count
        FROM HOLIDAY
        WHERE TRUNC(HOLIDAY_DATE) = TRUNC(PAYMENT_DATE);

        IF v_count > 0 THEN
            PAYMENT_DATE:= PAYMENT_DATE + 1;
        ELSE
            v_loop := 1;
        END IF;

    end loop;

    SELECT case
      when to_char(PAYMENT_DATE, 'fmday') = 'saturday' then
        PAYMENT_DATE + 2
      when to_char(PAYMENT_DATE, 'fmday') = 'sunday' then
        PAYMENT_DATE + 1
      else
        PAYMENT_DATE
    end AS PAYMENT_DATE
    INTO PAYMENT_DATE
    FROM DUAL;

    select 
      TRANS_FEE_PAY_T_RECORD(PAYEE_NAME,PAY_TYPE,BANK_ID,ACC_NO,PAYMENT_DATE,TRANSACTION_FEE)
    bulk collect into
      v_trans_fee_pay
    from 
      (SELECT PAYEE_NAME,PAY_TYPE,BANK_ID,ACC_NO,PAYMENT_DATE,TRANSACTION_FEE FROM DUAL);

    return v_trans_fee_pay;

end F_GET_TRANS_FEE_PAY;

/

SHOW ERRORS;

CREATE OR REPLACE FUNCTION F_GET_TRANS_FEE_HEADER_CHECK (
    p_TRANS_FEE_HEADER_ID in number
)
return TRANS_FEE_CHECK_T_TABLE as
    v_trans_fee_check   TRANS_FEE_CHECK_T_TABLE;
    CURRENT_TRANSACTION_FEE number;
    CURRENT_TRANSACTION_FEE_ALL number;
    TRANS_EMPYEE_COUNT number;
    IS_PASS_1 number:=0;
    IS_PASS_2 number:=0;
    CONDITION_CHECK_DETIAL_1 varchar2(1000);
    CONDITION_CHECK_DETIAL_2 varchar2(1000);
begin

    SELECT TBL_VALUE INTO CURRENT_TRANSACTION_FEE FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'TRANSACTION_FEE';
    SELECT TBL_VALUE_3 INTO CONDITION_CHECK_DETIAL_1 FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'CONDITION_CHECK_DETIAL_1';
    SELECT TBL_VALUE_3 INTO CONDITION_CHECK_DETIAL_2 FROM TABLE_LINE WHERE TBL_CODE = 'TRANS_FEE' AND TBL_ID = 'CONDITION_CHECK_DETIAL_2';

    SELECT COUNT(EMPYEE_ID)
    INTO TRANS_EMPYEE_COUNT
    FROM (SELECT DISTINCT EMPYEE_ID
    FROM Trans_Fee_Detail WHERE Trans_Fee_Header_id = p_TRANS_FEE_HEADER_ID
    GROUP BY EMPYEE_ID);

    CURRENT_TRANSACTION_FEE_ALL := NVL(CURRENT_TRANSACTION_FEE,0) * TRANS_EMPYEE_COUNT;

    select 
      TRANS_FEE_CHECK_T_RECORD
      (
      TRANS_FEE_HEADER_ID,
      FUND_ID,
      FUND_CODE,
      TRANS_EMPYEE_COUNT,
      TRANSACTION_FEE,
      AMOUNT,
      TRANS_FEE_STATUS,
      IS_PASS,
      IS_PASS_1,
      IS_PASS_2,
      IS_PASS_1_DETAIL,
      IS_PASS_2_DETAIL,
      CURRENT_TRANSACTION_FEE,
      CURRENT_TRANSACTION_FEE_ALL
      )
    bulk collect into
      v_trans_fee_check
        FROM 
        (
        SELECT
        TF.Trans_Fee_Header_id AS TRANS_FEE_HEADER_ID,
        TF.FUND_ID AS FUND_ID,
        F.FUND_CODE AS FUND_CODE,
        TRANS_EMPYEE_COUNT,
        TF.TRANSACTION_FEE AS TRANSACTION_FEE,
        TF.AMOUNT AS AMOUNT,
        TF.Trans_Fee_STATUS AS TRANS_FEE_STATUS,
        '' AS IS_PASS,
        CASE WHEN CURRENT_TRANSACTION_FEE_ALL = TF.AMOUNT THEN 1 ELSE 0 END AS IS_PASS_1,
        (select CASE WHEN COUNT(*) > 0 THEN 0 ELSE 1 END from table(F_GET_TRANS_FEE_HEADER_CHECK_DE(TF.Trans_Fee_Header_id))) AS IS_PASS_2,
        CONDITION_CHECK_DETIAL_1 AS IS_PASS_1_DETAIL,
        CONDITION_CHECK_DETIAL_2 AS IS_PASS_2_DETAIL,
        CURRENT_TRANSACTION_FEE AS CURRENT_TRANSACTION_FEE,
        CURRENT_TRANSACTION_FEE_ALL AS CURRENT_TRANSACTION_FEE_ALL
      FROM Trans_Fee_Header TF
      INNER JOIN FUND F ON F.FUND_ID = TF.FUND_ID
      WHERE tf.trans_fee_header_id = p_TRANS_FEE_HEADER_ID
    );

    return v_trans_fee_check;

end F_GET_TRANS_FEE_HEADER_CHECK;

/

SHOW ERRORS;

EXIT;