-- BEGIN SCRIPT --

Insert into MENU_APP_KA
(APP_ID, MENU_ID, MENU_NAME, SEQ_MENU, LEVEL_MENU, 
MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, HAS_FUNC_VIEW, 
HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000020, 'Disbursement', 2000, 20, 
	0, 'Y', 'Y', 'Y', 'Y', 
	'Y', '1', 'Y', 'fas fa-hand-holding-usd');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000040, '�ͤ׹�Թ�����Թ', '/k-pvdRegistra/#/pages/over-con-refund', 2000, 
	40, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000060, '͹��ѵԢͤ׹�Թ�����Թ', '/k-pvdRegistra/#/pages/approve-over-con-refund', 2000, 
	60, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000080, '�ͤ׹�Թ����ҼԴ�ѭ��', '/k-pvdRegistra/#/pages/reimbursement/search', 2000, 
	80, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000100, '͹��ѵԢͤ׹�Թ����ҼԴ�ѭ��', '/k-pvdRegistra/#/pages/reimbursement/approve', 2000, 
	100, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000120, '��Ǩ�ͺ��èѴ�����¡��', '/k-pvdRegistra/#/pages/processing-validation', 2000, 
	120, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000160, 'Head Section Approve', '/k-pvdRegistra/#/pages/head-section-approve/HeadSectionApprove', 2000, 
	160, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000180, '��Ǩ�ͺ���͹��ѵ���¡�è����Թ', '/k-pvdRegistra/#/pages/final-validation/FinalApprove', 2000, 
	180, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000200, '��Ҹ������������Թ�Ǵ', '/k-pvdRegistra/#/pages/fee-validation/StaffSearch', 2000, 
	200, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000240, '���ը�����þҡ�', '/k-pvdRegistra/#/pages/tax-validation/StaffSearch', 2000, 
	240, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000260, '��Ǩ�ͺ���ը�����þҡ�', '/k-pvdRegistra/#/pages/tax-validation/ManagerApprove', 2000, 
	260, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 2000000220, '��Ǩ�ͺ��Ҹ������������Թ�Ǵ', '/k-pvdRegistra/#/pages/fee-validation/ManagerApprove', 2000, 
	220, 2000000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');
Insert into PVDREGISTRA_TEST.MENU_APP_KA
   (APP_ID, MENU_ID, MENU_NAME, NAVIGATION_URL, SEQ_MENU, 
	LEVEL_MENU, MAIN_MENU_ID, HAS_FUNC_ADD, HAS_FUNC_EDIT, HAS_FUNC_DELETE, 
	HAS_FUNC_VIEW, HAS_FUNC_APPROVE, IS_SEPERATE, IS_NEW_APP, MENU_ICON)
 Values
   ('PVD', 400000420, '��˹��͡���', '/k-pvdRegistra/#/pages/master-data/setup-document', 400, 
	420, 400000020, 'Y', 'Y', 'Y', 
	'Y', 'Y', '1', 'Y', 'fas fa-circle');


---------------  Update Menu Icon --------------- 
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-circle' WHERE MAIN_MENU_ID != '2000000020' and MAIN_MENU_ID != '0';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-money-check-alt' WHERE APP_ID = 'PVD' AND MENU_ID = '1700000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-file-signature' WHERE APP_ID = 'PVD' AND MENU_ID = '1800000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-chart-pie' WHERE APP_ID = 'PVD' AND MENU_ID = '1900000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-hand-holding-usd' WHERE APP_ID = 'PVD' AND MENU_ID = '2000000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-user-cog' WHERE APP_ID = 'PVD' AND MENU_ID = '100000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-search' WHERE APP_ID = 'PVD' AND MENU_ID = '200000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-sliders-h' WHERE APP_ID = 'PVD' AND MENU_ID = '300000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-cogs' WHERE APP_ID = 'PVD' AND MENU_ID = '400000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-newspaper' WHERE APP_ID = 'PVD' AND MENU_ID = '700000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-warehouse' WHERE APP_ID = 'PVD' AND MENU_ID = '1000000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-pager' WHERE APP_ID = 'PVD' AND MENU_ID = '1500000020';
UPDATE MENU_APP_KA SET IS_NEW_APP = 'N', MENU_ICON = 'fas fa-tasks' WHERE APP_ID = 'PVD' AND MENU_ID = '1600000020';

COMMIT;

EXIT;
