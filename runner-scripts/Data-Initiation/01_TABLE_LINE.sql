-- BEGIN SCRIPT --

---------------------  APPROVE_TRAN_TYPE ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'VTPA', '�ӹǹ Transaction', '0', 'CM', 
    'N', 'VTPA', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'VEST', '�Թ����ҳ�����º�Ѻ�Թ����ͧ���·�����', '0', 'CM', 
    'N', 'VEST', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'VTAX', '���ը�����þҡ� �Ѻ���ͧ�ѭ��', '0', 'CM', 
    'N', 'VTAX', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'VCNF', '�����ŢѴ���', '0', 'CM', 
    'N', 'VCNF', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3)
 Values
   ('APPROVE_TRAN_TYPE', 'VUNV', '�Թ Unvested', '0', 'CM', 
    'N', 'VUNV');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'TRANSACTION', '�ӹǹ Transaction', '0', 'MP', 
    1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'ESTIMATE', '�Թ����ҳ�����º�Ѻ�Թ����ͧ���·�����', '0', 'MP', 
    2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'TAX', '���ը�����þҡ� �Ѻ���ͧ�ѭ��', '0', 'MP', 
    3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CONFLICT', '�����ŢѴ���', '0', 'MP', 
    4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'OVCN', '�ͤ׹�Թ����', '0', 'NU', 
    'Y', 'OVCN', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'RMBP', 'Reimbursement', '0', 'NU', 
    'Y', 'RMBP', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CHGP', '����¹�ŧ��è����Թ', '0', 'NU', 
    'Y', 'PYMP', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CHOV', '�礤ú��˹� 6 ��͹ ���Ǣ��͡������', '0', 'NU', 
    'Y', 'PYMP', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'APBP', '�� Over Due �礤�ҧ�����Թ 6 ��͹', '0', 'NU', 
    'Y', 'APBP', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'PFEE', '��Ҹ��������Թ�Ǵ', '0', 'PF', 
    'Y', 'PFEE', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'PTAX', '�ѹ�Թ����', '0', 'PF', 
    'N', 'PTAX', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CTAX', '������þҡ�', '0', 'PF', 
    'Y', 'CTAX', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CTNF', '���͡����ҾẺ�͹���������ͧ�ع���ǡѹ', '0', 'TP', 
    'N', 'RLSP', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CLMT', '���͡����ҾẺ�͹����', '0', 'TP', 
    'N', 'RLSP', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'TNFF', '���͡����Ҿ� �ͧ�ع��������Ũ. ���ǡѹ', '0', 'TP', 
    'Y', 'RLSP', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'TRMF', '�͹����� RMF', '0', 'TP', 
    'Y', 'RLSP', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CLMS', '���͡����Ҫԡ�ҾẺ���Թ', '0', 'TP', 
    'N', 'RLSP', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CAET', '���͡����ҾẺ�Ѻ�Թ�Ǵ', '0', 'TP', 
    'Y', 'RLSP', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'OCAL', 'Over Con. Ẻ�Ѵ���', '0', 'TP', 
    'Y', 'RLSP', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'TNFO', '���͡����Ҿ� �Ũ.���', '0', 'TP', 
    'Y', 'RLSP', 8);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'CLMA', '���͡����Ҿ', '0', 'TP', 
    'Y', 'RLSP', 9);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, 
    TBL_VALUE_2, TBL_VALUE_3, TBL_SEQ)
 Values
   ('APPROVE_TRAN_TYPE', 'UNVE', 'Unvest ���¤׹��¨�ҧ', '0', 'TP', 
    'Y', 'RLSP', 10);


---------------------  CONFLICT_ERROR ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '1', '���ͼ���Ѻ�Ż���ª��ͧ���͡�������ç�Ѻ������Ҫԡ', '0', '���ͼ���Ѻ�Ż���ª��ͧ���͡�������ç�Ѻ������Ҫԡ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '2', '�Ţ���ѭ�բͧ����Ѻ�͹���ç�Ѻ����к����� Profile �óչ�¨�ҧ�к��Ţ���ѭ��', '0', '�Ţ���ѭ�բͧ����Ѻ�͹���ç�Ѻ����к����� Profile �óչ�¨�ҧ�к��Ţ���ѭ��');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '3', '�ó��͹����仡ͧ�ع��� �� Profile �ͧ�ع����', '0', '�ó��͹����仡ͧ�ع��� �� Profile �ͧ�ع����');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '4', '�ѹ���������Ҫԡ ���اҹ ������ءͧ�ع �Ѵ��駡ѹ', '0', '�ѹ���������Ҫԡ ���اҹ ������ءͧ�ع �Ѵ��駡ѹ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '5', '��辺�����ŷ������ / �ٻẺ E-mail ���١��ͧ �ͧ��Ҫԡ���Թ / �Ѻ�Թ�Ǵ', '0', '��辺�����ŷ������ /  �ٻẺ E-mail ���١��ͧ �ͧ��Ҫԡ���Թ / �Ѻ�Թ�Ǵ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '6', '��Ǩ�ͺ�Ţ���ѭ�ո�Ҥ�ü���Ѻ�Ż���ª���ӡѹ', '0', '��Ǩ�ͺ�Ţ���ѭ�ո�Ҥ�ü���Ѻ�Ż���ª���ӡѹ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '7', '��Ǩ�ͺ�ʹ�Թ�͹���¡��Ң�鹵��', '0', '��Ǩ�ͺ�ʹ�Թ�͹���¡��Ң�鹵�� X �ҷ (X ��͵���Ţ�ӹǹ�Թ��� Configure ��к�)');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '8', '���ͼ���Ѻ�Ż���ª��ó����ª��Ե�ç�Ѻ������Ҫԡ', '0', '���ͼ���Ѻ�Ż���ª��ó����ª��Ե�ç�Ѻ������Ҫԡ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('CONFLICT_ERROR', '9', '��Ǩ�ͺ�Ţ�ѭ�ո�Ҥ�ë�ӡѺ�Ţ���ѭ��������', '0', '��Ǩ�ͺ�Ţ�ѭ�ո�Ҥ�ë�ӡѺ�Ţ���ѭ��������');


---------------------  CONTRIBUTION_PART ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('CONTRIBUTION_PART', 'EE', '��Ҫԡ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('CONTRIBUTION_PART', 'EY', '��¨�ҧ', '0');


---------------------  DDL_Max_List ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('DDL_Max_List', 'NAV', '�ӹǹ����ʴ�� LIST', '0', '100');


---------------------  DocList ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '1', '������������¹�ŧ��������Ҫԡ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '10', 'Ẻ�駤������ʧ��ͤ��Թ
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '11', 'Ẻ����Ҫԡ����Ҫԡ�Ҿ
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '12', '��������Ҫԡ Con', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '13', '���ػ��¡�ù����Թ��ҡͧ�ع���ͧ����§�վ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '14', '㺹ӽҡ (Deposit Slip)', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '15', '�ͤ׹�Թ�ͧ�ع�������Թ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '16', 'Ẻ����Ҫԡ��ش��ù����Թ��ҡͧ�ع���Ǥ���', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '17', '㺹ӽҡ���������Ѻ��ҡͧ�ع���ͧ����§�վ (Special Deposit Slip)', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '18', '㺹ӽҡ Ẻ������͡��� (Pay - In ������͡���)', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '19', '����� / ��������', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '2', 'Ẻ���͹�����͡�ҡ�ͧ�ع
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '20', '��', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '21', '�������¹���͵��', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '22', ' �������¹����ʡ��.', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '23', '��§ҹ��û�Ъ��', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '24', 'Ẻ�������Ѥ���Ҫԡ����觵�駼���Ѻ����ª��', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '25', '�͡�������', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '26', '���Һѵû�Ъ���', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '27', '���� Book bank', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '28', '������ó�ѵ�', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '29', '���ҷ���¹��ҹ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '3', '������������¹��º�¡��ŧ�ع', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '30', '���������', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '31', '���������', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '32', '˹ѧ����ͺ�ӹҨ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '33', '˹ѧ����������Թ���', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '34', '�͡��� Over Resign', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '35', 'Ẻ�駤������ʧ�������¹�� 
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '36', 'Ẻ�駤������ʧ����͡������跴᷹��Ѻ���
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '37', '������ Non-Fin', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '38', '�駤������ʧ�����͡Ἱ���ŧ�ع����Ѥ��', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '39', 'Ẻ�駧Ǵ��è��¤�Ҩ�ҧ��Шӻ�', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '4', 'Ẻ�����������¹��º�¡��ŧ�ع', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '40', '���Ѥ��������Ҫԡ�ͧ�ع���ͧ����§�վ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '41', '������Ἱ�ͧ��Ҫԡ Non-Fin', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '42', '�������Ѻ�͹ AIMC', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '43', 'Ẻ���Ѻ�͹ AIMC', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '44', 'Ẻ���Ѻ�͹��Ҫԡ��ҡͧ�ع
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '45', 'Ẻ������¹�ŧ��������Ҫԡ
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '5', '����������Ҫԡ����Ҫԡ�Ҿ
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '6', 'Ẻ������¹�ŧ���͹䢡���Ѻ�Թ�繧Ǵ
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '7', 'Ẻ���Ѻ�Թ�繧Ǵ�ͧ��Ҫԡ������³����
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '8', 'Ẻ���Ѻ�Թ��褧���㹡ͧ�ع
', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('DocList', '9', '˹ѧ����Ѻ�ͧ���اҹ������Ҫԡ�ͧ�ع
-', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME11', 'Ẻ����Ҫԡ����Ҫԡ�Ҿ
', 'AME', 'Chanya', 
    TO_DATE('09/30/2019 17:12:13', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME12', '��������Ҫԡ Con', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'N', 'Y', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME15', '�ͤ׹�Թ�ͧ�ع�������Թ', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'N', 'Y', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME17', '㺹ӽҡ���������Ѻ��ҡͧ�ع���ͧ����§�վ (Special Deposit Slip)', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'N', 'Y', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME20', '��', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'Y', 'Y', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME21', '�������¹���͵��', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'Y', 'Y', 8);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME22', ' �������¹����ʡ��.', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'N', 'Y', 9);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME3', '������������¹��º�¡��ŧ�ع', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'N', 'Y', 10);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME37', '������ Non-Fin', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'Y', 'Y', 11);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'AME42', '�������Ѻ�͹ AIMC', 'AME', TO_DATE('08/28/2019 17:36:32', 'MM/DD/YYYY HH24:MI:SS'), 
    'Y', 'Y', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'CIP13', '���ػ��¡�ù����Թ��ҡͧ�ع���ͧ����§�վ', 'CIP', 'Chanya', 
    TO_DATE('09/30/2019 17:12:39', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'CIP14', '㺹ӽҡ (Deposit Slip)', 'CIP', 'Chanya', 
    TO_DATE('09/30/2019 17:12:39', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, UPDATE_DATETIME, 
    TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'CIP6', 'Ẻ������¹�ŧ���͹䢡���Ѻ�Թ�繧Ǵ
', 'CIP', TO_DATE('08/28/2019 17:35:44', 'MM/DD/YYYY HH24:MI:SS'), 
    'Y', 'N', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'CIP9', '˹ѧ����Ѻ�ͧ���اҹ������Ҫԡ�ͧ�ع
-', 'CIP', 'Chanya', 
    TO_DATE('09/30/2019 17:12:39', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'INC_EXP13', '���ػ��¡�ù����Թ��ҡͧ�ع���ͧ����§�վ', 'INC_EXP', 'Chanya', 
    TO_DATE('09/30/2019 17:13:40', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 'Y', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM1', '������������¹�ŧ��������Ҫԡ', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'Y', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM11', 'Ẻ����Ҫԡ����Ҫԡ�Ҿ
', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 14);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM12', '��������Ҫԡ Con', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM14', '㺹ӽҡ (Deposit Slip)', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 'Y', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM18', '㺹ӽҡ Ẻ������͡��� (Pay - In ������͡���)', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 8);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM2', 'Ẻ���͹�����͡�ҡ�ͧ�ع
', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM22', ' �������¹����ʡ��.', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 11);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM23', '��§ҹ��û�Ъ��', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'Y', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM24', 'Ẻ�������Ѥ���Ҫԡ����觵�駼���Ѻ����ª��', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 9);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM3', '�ͤ׹�Թ�ͧ�ع�������Թ', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'Y', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM30', '���������', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 10);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM31', '���������', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 12);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM37', '������ Non-Fin', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'N', 13);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'RIM5', '����������Ҫԡ����Ҫԡ�Ҿ
', 'RIM', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'N', 'Y', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2, TBL_SEQ)
 Values
   ('DocList', 'TRAIMC13', '���ػ��¡�ù����Թ��ҡͧ�ع���ͧ����§�վ', 'TRAIMC', 'Chanya', 
    TO_DATE('09/30/2019 17:14:03', 'MM/DD/YYYY HH24:MI:SS'), 'Y', 1);


---------------------  DocType ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2)
 Values
   ('DocType', 'RIM', 'Reimbursement', '0', 'Pranee', 
    TO_DATE('01/09/2020 15:51:34', 'MM/DD/YYYY HH24:MI:SS'), 'Y');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, USER_ID, 
    UPDATE_DATETIME, TBL_VALUE_2)
 Values
   ('DocType', 'TFN', 'Finance-dev', '0', 'Chanya', 
    TO_DATE('09/30/2019 16:49:49', 'MM/DD/YYYY HH24:MI:SS'), 'Y');


---------------------  ESTIMATE_PAY_COL_NAME ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'MS_FUND_CODE', '���ʡͧ�ع', '0', 0);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'POLICY_CODE', '���� Sub Fund', '1', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'MARK_UP_AMT', '�ʹ����ҳ���', '10', 10);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'OVERRESIGN_DONATE_AMT', '�ӹǹ�Թ���� Over Resign ����Թ��ԨҤ', '11', 11);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'ESTIMATE_PAY_TOTAL', '�ʹ�Թ����ҳ��è��·���觺ѭ��', '12', 12);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'IS_APPROVE_TO_USE', '���ʹ仺ѭ��', '13', 13);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'APPROVE_DATETIME', '�ѹ���͹��ѵ�', '14', 14);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'APPROVE_USER_ID', '������ʹ', '15', 15);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'AMC_SHORT_NAME', '���� �Ũ.', '2', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'NAV', 'NAV', '3', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'NAV_PER_UNIT', 'NAV/Unit', '4', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'CLAIM_AMT', '�ӹǹ�Թ���¾���Ҿ', '5', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'RETAIN_BALANCE_AMT', '�ӹǹ�Թ���¤��Թ', '6', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'RECEIVE_INSTALLMENT_AMT', '�ӹǹ�Թ�����Ѻ�Թ�Ǵ', '7', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'TRANSFER_IN_AMT', '�ӹǹ�Թ����Ѻ�͹', '8', 8);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_SEQ)
 Values
   ('ESTIMATE_PAY_COL_NAME', 'NET_AMT', 'Net Amount', '9', 9);


---------------------  Estimate ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('Estimate', '1', '% Unit Price (+)', '0', '1.5', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('Estimate', '2', '% Unit Price (-)', '0', '2.5', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('Estimate', '3', '% �ʹ����ҳ', '0', '5.5', 3);


---------------------  FLOWPAYMENTMNG_STATUS ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('FLOWPAYMENTMNG_STATUS', 'H', 'H=Head Section ��Ǩ�ͺ����׹�ѹ��¡�� (��¡�� Payment �١���ҧ)', '0', '�駼�����������', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('FLOWPAYMENTMNG_STATUS', 'I', '���ҧ Lot ���͵�Ǩ�ͺ', '0', '�ѹ�֡ Lot', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('FLOWPAYMENTMNG_STATUS', 'R', 'R=Reject', '0', '�������û���ʸ', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('FLOWPAYMENTMNG_STATUS', 'X', 'X=Delete', '0', 'ź Lot', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('FLOWPAYMENTMNG_STATUS', 'Z', 'Z=��������͹��ѵ�', '0', '��������͹��ѵ�', 3);


---------------------  MIN_AGE ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('MIN_AGE', '18', '���ط������Ҫԡ㹡ͧ�ع', '0', '18');


---------------------  MIN_TNF ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('MIN_TNF', '20', '��Ǩ�ͺ�ʹ�Թ�͹��鹵�ӹ��¡��� X �ҷ', '0', '20');


---------------------  OVERCON_STATUS ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('OVERCON_STATUS', '0', 'D : Draft', '0', 'D', 0);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('OVERCON_STATUS', '1', 'I : Initial', '0', 'I', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('OVERCON_STATUS', '2', 'M : Manager Approve', '0', 'M', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('OVERCON_STATUS', '3', 'H : Section Head Approve', '0', 'H', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('OVERCON_STATUS', '4', 'R : Reject', '0', 'R', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
Values
   ('OVERCON_STATUS', '5', 'X : Delete', '0', 'X', 5);


---------------------  OVERCON_TYPE ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('OVERCON_TYPE', 'EE', '�дѺ��Ҫԡ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('OVERCON_TYPE', 'EY', '�дѺ��¨�ҧ', '0');


---------------------  OverCon_Payment_Group ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('OverCon_Payment_Group', '1', '���ú��âͤ׹�Թ�Թ ����� 1', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('OverCon_Payment_Group', '2', '���ú��âͤ׹�Թ�Թ ����� 2', '0');


---------------------  PAY_TO ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('PAY_TO', 'EE', '��Ҫԡ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('PAY_TO', 'EY', '��¨�ҧ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('PAY_TO', 'OA', 'Own account �׹�����Ңͧ�Թ', '0');


---------------------  REIMBURSE_STATUS ---------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '0', 'D : Draft', '0', 'D', 0);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '1', 'I : Initial', '0', 'I', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '2', 'M : Manager Approve', '0', 'M', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '3', 'H : Section Head Approve', '0', 'H', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '4', 'R : Reject', '0', 'R', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('REIMBURSE_STATUS', '5', 'X : Delete', '0', 'X', 5);


---------------------  REIMBURSE_STATUS_DEFAULT --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('REIMBURSE_STATUS_DEFAULT', '1', 'Default �ͧʶҹз��˹�Ҩ�', '0', 'I');


---------------------  SUB_OBJECT_TRANS_TYPE --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('SUB_OBJECT_TRANS_TYPE', 'CLM', '���͡', '0', 'CLM', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('SUB_OBJECT_TRANS_TYPE', 'EYP', '���¤׹�Թ�Ǵ', '0', 'EYP', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('SUB_OBJECT_TRANS_TYPE', 'OVRS', 'Over Resign', '0', 'OVRS', 2);


---------------------  TAX_CAL_PARAMETER --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'AGE_YEAR', '���ص����Ҫԡ', '0', '55');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'MEMBERYEAR_TAX', '������Ҫԡ', '0', '5');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'REDUCE_BEGIN', 'Ŵ���͹(����) 100000', '0', '100000');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'REDUCE_BEGIN_LIMIT', 'Ŵ���͹��ǹ���', '0', '0.5');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'REDUCE_BEGIN_NO_LIMIT', 'Ŵ���͹��ӹǹ�շ��ӧҹ ', '0', '0.5');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'REDUCE_PER_YEAR', 'Ŵ���͹(����) 7000', '0', '7000');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'WORKINGDAY_TAX', '�ӹǹ�ѹ', '0', '183');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TAX_CAL_PARAMETER', 'WORKINGYEAR_TAX', '���اҹ ', '0', '5');


---------------------  TERMINATE_PAY_GROUP_EMP --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TERMINATE_PAY_GROUP_EMP', '11', '¡��ԡ�ͧ�ع', '0', 'N');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TERMINATE_PAY_GROUP_EMP', '16', '�͹����������', '0', 'N');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('TERMINATE_PAY_GROUP_EMP', '8', '��¨�ҧ�͹����͡�ҡ�ͧ�ع', '0', 'N');


---------------------  TRANS_FEE --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('TRANS_FEE', 'ACC_NO', '�Ţ���ѭ��', '0', '7992405828', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('TRANS_FEE', 'BANK_ID', '��Ҥ�á�ԡ���', '0', '004', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE_3, TBL_SEQ)
 Values
   ('TRANS_FEE', 'CONDITION_CHECK_DETIAL_1', '���͹�㹡�õ�Ǩ�ͺ', '0', '�����ŵ�駵�:,��Ҹ������������Թ�Ǵ�����Ҫԡ 100.00,�����Ţ�з���Ǩ�ͺ:,��Ҹ������������Թ�Ǵ�����Ҫԡ 120.00', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE_3, TBL_SEQ)
 Values
   ('TRANS_FEE', 'CONDITION_CHECK_DETIAL_2', '���͹�㹡�õ�Ǩ�ͺ', '0', '��Ǩ�ͺ�ӹǹ��Ҫԡ��ѧ�Ѵ��� ���º��º�Ѻ,�ӹǹ��Ҫԡ���¡�ä�Ҹ������������Թ�Ǵ', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('TRANS_FEE', 'PAYEE_NAME', '����ѷ��ѡ��Ѿ��Ѵ��áͧ�ع��ԡ���', '0', '����ѷ��ѡ��Ѿ��Ѵ��áͧ�ع��ԡ���', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('TRANS_FEE', 'PAY_TYPE', '��������è���', '0', 'TNF', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('TRANS_FEE', 'TRANSACTION_FEE', '��Ҹ������������Թ�Ǵ', '0', '100', 4);


---------------------  UnvestedMoneyGroup --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('UnvestedMoneyGroup', 'RE', '���¤׹��¨�ҧ', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('UnvestedMoneyGroup', 'RI', '�׹��¨�ҧ���й��ŧ�ع���', '0');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID)
 Values
   ('UnvestedMoneyGroup', 'RM', '�׹��Ҫԡ�������㹡ͧ�ع', '0');


---------------------  VERIFY_CONCURRENT --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_CONCURRENT', '1', '�ӹǹ Process ����Դ��鹾�����ѹ���٧', '0', '10');


---------------------  VERIFY_EMAIL --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_EMAIL', '0', '[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9._-]{2,}[.]{1}[a-zA-Z]{2,}', '0', '[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-_]{2,}[.]{1}[a-zA-Z]{2,}');


---------------------  VERIFY_ENDPOINT --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_ENDPOINT', 'ENDPOINT_CONFLICT', 'URL ����Ѻ�ӹǹ VerConflict ', '0', '/api/VerConflict/Calculate');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_ENDPOINT', 'ENDPOINT_ESTIMATE_PAY', 'URL ����Ѻ�ӹǹ VerEstimatePay', '0', '/api/VerEstimatePay/Calculate');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_ENDPOINT', 'ENDPOINT_TAX', 'URL ����Ѻ�ӹǹ VerifyTax', '0', '/api/VerifyTax/Calculate');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_ENDPOINT', 'ENDPOINT_TRANSACTION', 'URL ����Ѻ�ӹǹ VerTransaction', '0', '/api/VerTransaction/Calculate');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('VERIFY_ENDPOINT', 'ENDPOINT_UNVESTED', 'URL ����Ѻ�ӹǹ VerUnvested ', '0', '/api/VerUnvested/Calculate');


---------------------  VERIFY_STATUS --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'E', 'Error', '0', 'Error', 4);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'F', '����������', '0', '����������', 5);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'H', 'Section Head ͹��ѵ�', '0', 'Section Head ͹��ѵ�', 8);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'I', '�׹�ѹ��Ǩ��¡��', '0', '�׹�ѹ��Ǩ��¡��', 6);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'M', 'Manager ͹��ѵ�', '0', 'Manager ͹��ѵ�', 7);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'N', '�ѧ����Ǩ�ͺ', '0', '�ѧ����Ǩ�ͺ', 1);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'P', '���ѧ�����ż�', '0', '���ѧ�����ż�', 3);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'R', 'Reject', '0', 'Reject', 9);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'W', '�ͻ����ż�', '0', '�ͻ����ż�', 2);
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE, TBL_SEQ)
 Values
   ('VERIFY_STATUS', 'X', 'Delete', '0', 'Delete', 10);
   

---------------------  Verify Type --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('Verify Type', '1', '�Թ��������·�����', '0', '�Թ��������·�����');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('Verify Type', '2', '�Թ�����������Ҫԡ', '0', '�Թ�����������Ҫԡ');
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('Verify Type', '3', '�Թ��������¹�¨�ҧ', '0', '�Թ��������¹�¨�ҧ');
   

---------------------  overcon_payment_type_ee_payment_default --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('overcon_payment_type_ee_payment_default', 'CAP', '���������鹢ͧ��è����Թ�׹�дѺ��¨�ҧ', '0', '2');
      

---------------------  overcon_payment_type_ey_payment_default --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('overcon_payment_type_ey_payment_default', 'CAP', '���������鹢ͧ��è����Թ�׹�дѺ��Ҫԡ', '0', '1');
      

---------------------  payment_type_reimburse_default --------------------
Insert into TABLE_LINE
   (TBL_CODE, TBL_ID, TBL_DESC, TBL_MASTER_ID, TBL_VALUE)
 Values
   ('payment_type_reimburse_default', 'CAP', '���������鹢ͧ��è����Թ�׹�ͧ reimburse', '0', '1');
      

---------------------  Update UnvestedMoney  --------------------
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = '1C';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = '1M';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = '4P';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'BM';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'CU';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'MA';
UPDATE table_line SET TBL_MASTER_ID = 'RM' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'MC';
UPDATE table_line SET TBL_MASTER_ID = 'RE' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'RE';
UPDATE table_line SET TBL_MASTER_ID = 'RI' WHERE TBL_CODE = 'UnvestedMoney' AND TBL_ID = 'RI';

COMMIT;

EXIT;